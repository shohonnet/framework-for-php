<?php
$config['template'] = [
    'theme' => 'default',    //使用的主题
    'templateFileExtension' => '.html',    //模板文件扩展名
    'compileFileExtension' => '.phtml',    //编译文件扩展名
    'commonTemplateFolderName' => 'cmn',    // 公共模板编译文件所在文件夹名称
    'commonCompileFileExtension' => '.common.phtml',    //公共模版编译文件扩展名
    'autoCreateTemplateFile' => true
];