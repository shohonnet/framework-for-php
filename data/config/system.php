<?php
$config['system'] = [
    'wxApplet' => [
        'appid' => '',
        'appsecret' => ''
    ],
    'tencent' => [
        'sms' => [
            'secretId' => '',
            'secretKey' => ''
        ]
    ]
];