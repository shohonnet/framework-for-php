<?php
// 本地数据库
$config['database'] = [
    'type' => "mysql:pdo",
    'host' => '127.0.0.1',
    'port' => '3306',
    'user' => 'root',
    'dbname' => 'test',
    'password' => '',
    'charset' => 'utf8mb4',
    'prefix' => 'sh_',
    'drive' => 'PDO_MySQL'  // 目前仅支持MySQL
];