<?php
/**
 * Created by PhpStorm
 * User: Administrator
 * Date: 2022/11/27
 * Time: 3:05
 * docs:
 */

namespace Platform\Common\Module {

    use JetBrains\PhpStorm\NoReturn;
    use System\Lib\Module;
    use System\Lib\ModuleInterface;
    use System\Lib\Upload;
    use System\Tool\Dir;
    use System\Tool\Randstr;
    use WebGeeker\Validation\Validation;
    use WebGeeker\Validation\ValidationException;

    #[\AllowDynamicProperties]
    class Common extends Module implements ModuleInterface
    {
        protected static int $page = 1;
        protected static int $limit = 30;
        protected static array $where = [];
        protected array $primaryKey = ['id'];

        public function __construct()
        {
            parent::__construct();
            // 接收参数
            self::$page = $this->_getInputPostData('page', 1);
            self::$limit = $this->_getInputPostData('limit', 30);
            self::$where = $this->_getInputPostData('where', []);

            $this->tpl->assign('platform', $this->_getPlatform());
            $this->tpl->assign('module', $this->_getModule());
            $this->tpl->assign('action', $this->_getAction());
            $this->tpl->assign('randStr', Randstr::getCode(10));
        }

        /**
         * @param array $where_allow_field
         * @param $model
         * @param callable|null $call
         * @param callable|null $whereCall
         * @return void
         */
        #[NoReturn] protected function package_data(array $where_allow_field, $model, ?callable $call = null, ?callable $whereCall = null): void
        {
            $where_array = [];
            foreach ($where_allow_field as $key => $val){
                if(!empty(self::$where[$val])){
                    if(empty($whereCall)){
                        $where_array[] = [$val, self::$where[$val]];
                    }else{
                        $whereCall($val, $where_array);
                    }
                }
            }


            $list = $model->list(self::$page, self::$limit, $where_array);

            if(!empty($call)){
                $call($list);
            }

            $count = $model->count($where_array);

            foreach($this->primaryKey as $vo){
                if(!empty(self::$where[$vo])){
                    if(!empty($list)){
                        $list = $list[0];
                        break;
                    }
                }
            }

            $this->ajax->show('拉取数据成功',0, $list, $count);
        }

        /**
         * @name: check_data
         * @param callable|null $call
         * @return array|bool
         * @throws ValidationException
         * @author: Administrator
         * @Time: 2022/12/3 2:09
         * @Desc:
         */
        protected function check_data( callable $call = null) : array|bool
        {
            if(empty($this->validateParam))
                return [];

            try {
                Validation::validate($_POST, $this->validateParam);
                $data = [];
                foreach ($this->validateParam as $key => $val) {
                    $data[$key] = $this->_getInputPostData($key);
                }

                if($call){
                    $call($data);
                }

                return $data;
            }catch (ValidationException $e){
                $this->ajax->show($e->getMessage());
            }

        }

        /**
         * @param array $data 原始数据
         * @param string $field 匹配字段
         * @param string $value 匹配值
         * @param string $retField 返回字段
         * @return string|array
         * @author: Administrator
         * @Time: 2022/11/26 1:27
         * @Desc: 返回匹配的值
         */
        protected function getDataName(array &$data, string $field, string $value, string $retField) : string|array
        {
            foreach ($data as $vo) {
                if($vo[$field] == $value){
                    return $vo[$retField];
                }
            }
        }

        /**
         * 上传文件
         * @return void
         * @author Jimmy 2022-12-15
         */
        #[NoReturn] protected function uploadImage(): void
        {
            $uploadFile = Upload::getInstance()->set_max_size(1024 * 500)->set_allow_type(['image/jpg', 'image/png', 'image/jpeg', 'image/gif']);
            $uploadFile->start_receive();
            $uploadFile->start_move();
            $this->ajax->show('上传成功', 0, $uploadFile->files);
        }

        /**
         * @name: loadModel
         * @author: Administrator
         * @Time: 2022/8/24 11:42
         * @Desc: 加载当前平台下的所有模块
         */
        protected function loadModel() : void
        {
            $model_dir = getcwd() . _DS_ .  'platform' . _DS_ . $this->_getPlatform() . _DS_ . 'model';//dirname(__FILE__, 2) . _DS_ . 'model';
            $model_files = Dir::getFiles(path: $model_dir, contain: '.php');
            foreach($model_files as $mn){
                $model_name = str_replace('.php', '', $mn);
                $tmp_model_name = lcfirst($model_name) . 'Model';
                $model_class_name = '\\Platform\\' . $this->_getPlatform() . '\\Model\\' . $model_name;
                $this->$tmp_model_name = new $model_class_name();
            }
        }

        #[NoReturn] public function index()
        {
            $this->_404();
        }

        #[NoReturn] public function data()
        {
            // TODO: Implement data() method.
            $this->_404();
        }

        #[NoReturn] public function insert()
        {
            // TODO: Implement insert() method.
            $this->_404();
        }

        #[NoReturn] public function updata()
        {
            // TODO: Implement updata() method.
            $this->_404();
        }

        #[NoReturn] public function delete()
        {
            // TODO: Implement delete() method.
            $this->_404();
        }

        #[NoReturn] public function compile_param()
        {
            // TODO: Implement compile_param() method.
            $this->_404();
        }
    }
}