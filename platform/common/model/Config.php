<?php

namespace Platform\Common\Model{

    use System\Lib\Base;

    class Config extends Base
    {
        protected string $tableName = 'config';

        public function get($key): ?array
        {
            $returnDate = null;
            $settingInfo = $this->_DAO->setTable('config')->setWhere([['key', $key]])->queryOne();
            if(!empty($settingInfo)){
                $returnDate = $settingInfo;
            }
            return $returnDate;
        }

        public function set($key, $value): int
        {
            if($this->_DAO->setTable('config')->setWhere([['key', $key]])->count()){
                // 修改
                return $this->_DAO->setTable('config')->setWhere([['key', $key]])->setData(['value' => $value, 'updatetime' => time()])->update();
            }else{
                // 添加
                return $this->_DAO->setTable('config')->setData(['key' => $key, 'value' => $value, 'updatetime' => time()])->insert();
            }
        }
    }
}