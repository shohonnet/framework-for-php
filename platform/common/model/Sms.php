<?php
namespace Platform\Common\Model {

    use System\Lib\Tencent\Sms as TencentSms;

    class Sms
    {
        private TencentSms $smsObject;
        private string $appid = '';
        private string $sign = '';

        public function __construct()
        {
            $this->smsObject = new TencentSms();
        }

        /**
         * 发送登录验证码
         * @param $phone
         * @param $code
         * @return mixed
         * @author Administrator 2022-12-20
         */
        public function sendLoginCheckCode($phone, $code) : mixed
        {
            return $this->smsObject->send(phone: ['+86' . $phone], sign: $this->sign, param: [$code], template: '1643223', appid: $this->appid);
        }
    }
}