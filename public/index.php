<?php
const _DS_ = DIRECTORY_SEPARATOR;
chdir(is_dir(dirname(__DIR__) . _DS_ . 'public') ? "../" : './');
include_once('vendor/autoload.php');
use System\Core\Core;

if (isset($argv) && count($argv)) {
    $_GET['s'] = $argv[1] ?? '';
}
Core::init();
