# framework for php v1.0
## 推荐编辑器
`PhpStorm`

## 更新时间
`2023/05/11 17:45`

## 介绍
>[四川硕航网络科技有限公司](https://www.saaon.com '四川硕航网络科技有限公司') 开发的快速、高效php开发框架。采用OOP（面向对象）进行设计和研发。

## 软件架构
`MVC` `PHP` `OOP`

## 安装教程
### 拉取代码
    composer create-project shuohang/ffp

### 安装扩展包
    composer install

## 目录结构
```
| -- data
| -- | -- config           配置文件夹
| -- platform              平台文件夹
| -- public                静态资源文件夹
| -- | -- index.php        入口文件
| -- system                框架核心
| -- | -- core
| -- | -- font
| -- | -- lib
| -- | -- tool
```

## 使用说明
### 模块

#### 简单模块
```php
class Index{
    public function index(){
        echo '简单模块'
    }
}
```

#### 继承模块
```php
class Index extends \System\Lib\Module {
    public function index(){
        echo '继承模块'
    }
}
```

#### 公共模块（继承自框架级基础模块）
```php
# 文件：Common.php
class Common extends \System\Lib\Module{
    public function __construct() {
        parent::__construct();
        # 业务逻辑....
    }
}

# 文件：Index.php
class Index extends Common{
    public function index(){
        echo '公共模块（继承自框架级基础模块）'
    }
}
```

#### 公共模块（继承自系统级别基础模块）<font style="color:red; font-weight: bold">！！！！！！！！！！！！推荐</font>
```php
# 文件：Common.php
class Common extends \Platform\Common\Module\Common {
    public function __construct() {
        parent::__construct();
        # 业务逻辑....
    }
}

# 文件：Index.php
class Index extends Common{
    public function index(){
        echo '公共模块（继承自系统级别基础模块）'
    }
}
```

### 模型

#### 简单模型
```php
class User extends \System\lib\Model{
    public function list(int $page = 1, int $limit = 10, array $where = []): array
    {
        return $this->_DAO->setTable('user', 'A')->setPage($page, $limit)->setWhere(...$where)->setOrder($this->orderField)->query();
    }

    /**
     * @param array $where
     * @return int
     */
    public function count(array $where = []): int
    {
        return $this->_DAO->setTable('user', 'A')->setWhere(...$where)->count();
    }

    /**
     * @param array $where
     * @return array
     */
    public function info(array $where): array
    {
        return $this->_DAO->setTable('user', 'A')->setWhere(...$where)->first();
    }

    /**
     * @param array $data
     * @return int
     */
    public function insert(array $data): int
    {
        return $this->_DAO->setTable('user', 'A')->setData($data)->insert();
    }

    /**
     * @param array $data
     * @param array $where
     * @return int
     */
    public function update(array $data, array $where): int
    {
        return $this->_DAO->setTable('user', 'A')->setData($data)->setWhere(...$where)->update();
    }

    /**
     * @param array $where
     * @return int
     */
    public function delete(array $where) : int
    {
        return $this->_DAO->setTable('user', 'A')->setWhere(...$where)->delete();
    }
}
```

#### 极简模型 <font style="color:red; font-weight: bold">！！！！！！！！！！！！推荐</font>
```php
# 这样就已经继承好了基础的增删改查方法，可直接使用
class User extends \System\Lib\Base{
    protected string $tableName = 'user';
    protected array $orderField = ['id' => 'desc'];
}
```

## 框架工具说明
今天累了，以后再写。。。。。