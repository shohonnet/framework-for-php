<?php
namespace System\Core {

    class Tool{
        public static function dump(): void
        {
            $track = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

            $args = func_get_args();

            foreach ($args as $arg) {
                echo "<pre>" . $track[0]['file'] . "：" . $track[0]['line'] . "\r\n";
                if (is_array($arg)) {
                    print_r($arg);
                } else {
                    var_dump($arg);
                }
                echo "</pre>";
            }
        }

        public static function reqFile($fileDir): void
        {
            if(file_exists($fileDir)){
                include_once $fileDir;
            }else{
                Debug::console('文件【' . $fileDir . '】不存在！', false);
            }
        }

        
    }
}