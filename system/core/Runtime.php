<?php
namespace System\Core {

    use JetBrains\PhpStorm\ArrayShape;
    use Whoops\Handler\PrettyPageHandler;
    use Whoops\Run;

    class Runtime{
        // 错误处理状态
        protected static int $state = 0;

        private function __construct()
        {
        }

        /**
         * @param mixed $errno
         * @return array
         */
        #[ArrayShape(['error' => "string", 'log' => "int|string"])] public function getError(mixed $errno): array
        {
            $error = $log = '';

            switch ($errno) {
                case E_PARSE:
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                    $error = 'Fatal Error';
                    $log = LOG_ERR;
                    break;
                case E_WARNING:
                case E_USER_WARNING:
                case E_COMPILE_WARNING:
                case E_RECOVERABLE_ERROR:
                    $error = 'Warning';
                    $log = LOG_WARNING;
                    break;
                case E_NOTICE:
                case E_USER_NOTICE:
                    $error = 'Notice';
                    $log = LOG_NOTICE;
                    break;
                case E_STRICT:
                    $error = 'Strict';
                    $log = LOG_NOTICE;
                    break;
                case E_DEPRECATED:
                case E_USER_DEPRECATED:
                    $error = 'Deprecated';
                    $log = LOG_NOTICE;
                    break;
                default :
                    break;
            }
            return ['error' => $error, 'log' => $log];
        }

        private function __clone()
        {
            // TODO: Implement __clone() method.
        }

        private static Runtime|null $instance;

        public static function init() : ?Runtime
        {
            if(self::$state == 1){
                return null;
            }
            self::$instance = null;
            if(!self::$instance instanceof self){
                self::$instance = new self();
                
                set_exception_handler([self::$instance, 'errorHandle']);
                set_error_handler([self::$instance, 'errorHandle']);
                register_shutdown_function([self::$instance, 'errorHandle']);
            }



            return self::$instance;
        }

        public static function errorHandle(): void
        {

        }

        public function runtime_exception_callback($error): void
        {
            Debug::error($error->getMessage(), ['错误文件：' . $error->getFile() . ':' .  $error->getLine()], $error->getTrace(), false);
        }

        public function runtime_error_callback(int $errno, string $errstr ,string $errfile, int $errline): void
        {
            $errorArr = $this->getError($errno);
            $error = $errorArr['error'];
            $log = $errorArr['log'];

            if(Config::init()->__get('debug')['only_fatal_error'] && $error == 'Fatal Error'){
                Debug::error("【{$error}】" . $errstr, ['错误文件：' . $errfile . ':' . $errline], false, true);
                // trigger_error('【{$error}】', E_USER_ERROR);
            }else{
                 Debug::error("【{$error}】" . $errstr, ['错误文件：' . $errfile . ':' . $errline], false, $error == "Fatal Error");
            }
        }

        public function runtime_fatal_callback(): void
        {
            $lastError = error_get_last();
            if($lastError != null){

                $errno = $lastError['type'];

                $errorArr = $this->getError($errno);
                $error = $errorArr['error'];
                $log = $errorArr['log'];

                Debug::error("【{$error}】" . $lastError['message'], ['错误文件：' . $lastError['file'] . ':' . $lastError['line']], false, true);
            }
        }
    }
}