<?php
namespace System\Core {

    use System\Tool\Common;
    use Whoops\Handler\CallbackHandler;
    use Whoops\Handler\JsonResponseHandler;
    use Whoops\Handler\PlainTextHandler;
    use Whoops\Handler\PrettyPageHandler;
    use Whoops\Run;

    class Core{
        protected static mixed $instance = null;

        private function __construct() {
        }

        private function __clone(): void {
            // TODO: Implement __clone() method.
        }

        public static function init() {
            if(!self::$instance instanceof self)
                self::$instance = new self();
            self::$instance->prepare();
            return self::$instance;
        }

        private function prepare(): void
        {
            // 类自动加载
            include_once  dirname(__FILE__) . '/AutoLoad.php';
            AutoLoad::init();
            // 加载路由
            $this->loadRoute();
            // 加载配置文件
            $this->loadConfig();

            // 加载错误处理
            $config = Config::init()->__get('debug');
            if($config['display_errors']){
                ini_set("display_errors","on");
                if($config['only_fatal_error']){
                    error_reporting(E_ERROR | E_USER_ERROR | E_CORE_ERROR | E_COMPILE_ERROR);
                }else{
                    error_reporting(E_ALL);
                }
                $whoops = new Run;
                $whoops->pushHandler(new PrettyPageHandler);
                $whoops->register();
            } else {
                ini_set("display_errors", "off");
            }

            $module = "Platform\\" . ucfirst(Route::$platform) . "\\Module\\" . ucfirst(Route::$module);
            $action = Route::$action;

            try {
                // 利用反射实例化类
                $refClass = new \ReflectionClass($module);
                $instance = $refClass->newInstanceArgs();
                // 调用方法
                $method = $refClass->getMethod($action);
                $method->invoke($instance);
            } catch (\ReflectionException $e) {
                trigger_error($e->getMessage(), E_USER_ERROR);
            }

        }

        private function loadConfig(): void
        {
            Config::init();
        }

        private function loadRuntime(): void
        {
            Runtime::init(Config::init()->__get('debug'));
        }

        private function loadRoute(): void
        {
            Route::init(Config::init()->__get('route'));
        }
    }
}