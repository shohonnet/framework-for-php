<?php
namespace System\Core{

    /**
     * 自动加载类
     */
    class AutoLoad{

        private static mixed $instance;

        private function __construct() {}

        private function __clone() {}

        public static function init(): void
        {
            spl_autoload_register([__NAMESPACE__ . '\AutoLoad', 'autoload']);
        }

        private static function autoload($className): void
        {
            $filePath = str_replace("\\", '/', $className);
            $filePathArray = explode("/", $filePath);
            foreach($filePathArray as $k => $v){
                if((count($filePathArray) - 1) != $k){
                    $filePathArray[$k] = strtolower($v);
                }
            }
            $fileDir = implode('/', $filePathArray) . '.php';
            if(!file_exists($fileDir)){
                Debug::error('引入文件时文件不存在',["文件路径：{$fileDir}"], debug_backtrace(), true);
                  // trigger_error('引入文件时文件不存在',E_USER_ERROR);
            }else{
                include_once $fileDir;
            }
        }
    }
}