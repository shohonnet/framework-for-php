<?php
namespace System\Core {

    use const _DS_;

    class Config{
        private readonly string $confDir;
        private static ?self $instance = null;
        private array $database = [];
        private array $debug = [];
        private array $route = [];
        private array $template = [];
        private array $system = [];
        private array $sm4 = [];
        private int $state = 0;
        private array $allowField = ['database','debug','route','template','system','sm4'];

        private function __construct()
        {
            $this->confDir = 'data' . _DS_ . 'config' . _DS_;
        }

        private function __clone()
        {
            // TODO: Implement __clone() method.
        }

        /**
         * @return static
         */
        public static function init() : self
        {
            if(!self::$instance instanceof self){
                self::$instance = new self();
            }

            if(self::$instance->state == 1){
                return self::$instance;
            }

            $config = [];
            // 引入所有配置文件
            foreach (self::$instance->allowField as $name){
                include_once self::$instance->confDir . $name . ".php";
                self::$instance->$name = $config[$name];
            }

            self::$instance->state = 1;
            return self::$instance;
        }

        public function __get(string $name)
        {
            // TODO: Implement __get() method.
            if(!in_array($name, $this->allowField))
                Debug::console("获取配置被禁止，【{$name}】项未被允许", 'true');

            return self::$instance->$name;
        }

        public function __set(string $name, array $value)
        {
            // TODO: Implement __set() method.
            if(!in_array($name, $this->allowField))
                Debug::console("更改配置被禁止，【{$name}】项未被允许",true);
            try{
                self::$instance->$name = array_merge(self::$instance->$name, $value);
                return true;
            }catch (\Exception $e){
                return false;
            }
        }

        public function __call(string $name, array $arguments)
        {
            trigger_error(__CLASS__ . "中不存在【" . $name . '】函数');
        }
    }
}