<?php
namespace System\Core {

    use system\tool\File;
    use system\core\Tool;

    final class Template{

        private string $themeName;
        private string $tplFileName;
        private string $tplFileDir;
        private string $compileFileDir;
        private string $commonTplFileDir;
        private string $commonCompileFileDir;
        private string $dirSep = '/';
        private string $tplContent = '';
        private array $config = [];
        public array $tpl_var = [];

        public function __construct()
        {
            // 更新配置
            $this->config = Config::init()->__get('template');
            // 站点主题名称
            $this->themeName = $this->config['theme'];
            // 模板文件名称
            $this->tplFileName = Route::$action . $this->config['templateFileExtension'];
            // 模板文件路径
            $this->pathDetermination();

            $this->setPageCharset();
        }

        private function changeTplName($name): void
        {
            // 站点主题名称
            $this->themeName = $this->config['theme'];
            // 模板文件名称
            $this->tplFileName = $name . $this->config['templateFileExtension'];
            // 模板文件路径
            $this->pathDetermination();
        }

        public function display($tplName = null): void
        {

            if(!empty($tplName)){
                $this->changeTplName($tplName);
            }
            // 检测文件夹是否存在，若不存在则直接创建
            if(!is_dir(dirname($this->tplFileDir))){
                mkdir(dirname($this->tplFileDir), 0700, true);
            }

            // 检测模板是否存在
            if(!file_exists($this->tplFileDir)){
                if($this->config['autoCreateTemplateFile']){
                    $platform_default_template_file_dir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'themes' . $this->dirSep . $this->themeName . $this->dirSep . $this->themeName .'.template.html';
                    if(file_exists($platform_default_template_file_dir)){
                        $default_content = file_get_contents($platform_default_template_file_dir);
                        $default_content = str_replace('{{PLATFORM}}', Route::$platform, $default_content);
                        $default_content = str_replace('{{MODULE}}', Route::$module, $default_content);
                        $default_content = str_replace('{{ACTION}}', ($tplName ?? Route::$action), $default_content);
                        $default_content = str_replace('{{THEME}}', $this->config['theme'], $default_content);
                        file_put_contents($this->tplFileDir, $default_content);
                    }
                }else{
                    Debug::error(__CLASS__ . ' Error：模板文件不存在', ['模板文件地址：' . $this->tplFileDir], false, true);
                }
            }

            $this->prepare();
            $this->checkSource();
            $this->tplContent = File::getFileContent($this->tplFileDir);
            $this->view();
        }

        public function display_import($tplName): void
        {
            $tplDir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'themes' . $this->dirSep . $this->themeName . $this->dirSep . Route::$module . $this->dirSep . $tplName . '.vue';
            // 检测模板是否存在
            if(!file_exists($tplDir)){
                Debug::error(__CLASS__ . ' Error：引入模板文件不存在', ['模板文件地址：' . $tplDir], false, true);
            }

            include_once $tplDir;
        }

        public function display_common($tplName): void
        {
            $tplDir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'themes' . $this->dirSep . $this->themeName . $this->dirSep . 'cmn' . $this->dirSep . $tplName . '.html';
            // 检测模板是否存在
            if(!file_exists($tplDir)){
                Debug::error(__CLASS__ . ' Error：公共模板文件不存在', ['模板文件地址：' . $tplDir], false, true);
            }

            TemplateFactory::compile($this->commonTplFileDir . $this->dirSep . $tplName . $this->config['templateFileExtension'],$this->commonCompileFileDir . $this->dirSep . md5(Route::$platform . $tplName) . $this->config['commonCompileFileExtension']);
            include_once $this->commonCompileFileDir . $this->dirSep . md5(Route::$platform . $tplName) . $this->config['commonCompileFileExtension'];
        }

        /**
         * 检测资源文件以及资源文件夹是否存在
         * @return void
         */
        private function checkSource(): void
        {
            $source_file = [
                'css' . $this->dirSep . Route::$platform . $this->dirSep . $this->themeName . $this->dirSep . Route::$module . $this->dirSep . Route::$action . '.css', //当前平台当前皮肤目录
                'js' . $this->dirSep . Route::$platform . $this->dirSep . $this->themeName . $this->dirSep . Route::$module . $this->dirSep . Route::$action . '.js', //当前平台当前皮肤目录
            ];

            foreach ($source_file as $v){

                if(!is_dir(dirname($_SERVER['DOCUMENT_ROOT'] . $this->dirSep . $v))){
                    mkdir(dirname($_SERVER['DOCUMENT_ROOT'] . $this->dirSep . $v), 0757, true);
                }

                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $this->dirSep . $v)){
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . $this->dirSep . $v, '');
                }
            }
        }

        /**
         * @param string $key 名称
         * @param mixed $value 值
         * @return void
         */
        public function assign(string $key, mixed $value): void
        {
            $this->tpl_var[$key] = $value;
        }

        private function view(): void
        {
            include_once $this->compileFileDir;
        }

        private function prepare(): void
        {
            // 检测编译文件目录是否存在
            if(!is_dir(dirname($this->compileFileDir))){
                mkdir(dirname($this->compileFileDir), 0757, true);
            }

            // 检测模板目录是否存在
            if(!is_dir(dirname($this->tplFileDir))){
                mkdir(dirname($this->tplFileDir), 0757, true);
            }

            // 检测文件是否需要编译 && 检测模板文件和编译文件的最后修改时间
            if(!file_exists($this->tplFileDir) || !file_exists($this->compileFileDir)){
                TemplateFactory::compile($this->tplFileDir, $this->compileFileDir);
            }else{
                if(filemtime($this->tplFileDir) > filemtime($this->compileFileDir) || (time() - 3600) > filemtime($this->compileFileDir)){
                    TemplateFactory::compile($this->tplFileDir, $this->compileFileDir);
                }
            }
        }

        public function setPageCharset(string $char = 'utf-8'): void
        {
            header('content-type:text/html;charset=' . $char);
        }

        /**
         * ${CARET}
         * @return void
         * @author Jimmy 2022-12-15
         */
        protected function pathDetermination(): void
        {
            $this->tplFileDir = dirname($_SERVER['DOCUMENT_ROOT']) . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'themes' . $this->dirSep . $this->themeName . $this->dirSep . Route::$module . $this->dirSep . $this->tplFileName;
            // 编译文件名称
            $compileFileName = md5($this->tplFileName) . $this->config['compileFileExtension'];
            // 编译文件路径
            $this->compileFileDir = dirname($_SERVER['DOCUMENT_ROOT']) . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'compile' . $this->dirSep . $this->themeName . $this->dirSep . Route::$module . $this->dirSep . $compileFileName;
            // 公共模板文件夹路径
            $this->commonTplFileDir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'themes' . $this->dirSep . $this->themeName . $this->dirSep . $this->config['commonTemplateFolderName'];
            // 公共编译文件夹路径
            $this->commonCompileFileDir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'compile' . $this->dirSep . $this->themeName . $this->dirSep . $this->config['commonTemplateFolderName'];

            if (!is_dir($this->commonCompileFileDir)) {
                mkdir($this->commonCompileFileDir, 0757, true);
            }
        }

    }

}