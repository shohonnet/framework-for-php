<?php
namespace System\Core {

    class Route{
        // 错误处理状态
        protected static int $state = 0;
        public static array $_GET = [];
        public static array $_POST = [];
        public static string $default_platform = 'home';
        public static string $platform = 'home';
        public static string $module = '';
        public static string $action = '';

        private function __construct($route_config)
        {
            $s = $_GET['s'] ?? '';

            $s = preg_replace("/(\/){2,+}/i", '/', $s);
            unset($_GET['s']);
            $this->SafeFilter($_GET);
            $this->SafeFilter($_POST);

            foreach($route_config as $v){
                if(preg_match($v[0], $s)){
                    $s = preg_replace($v[0], $v[1], $s);
                    if($v[2]){
                        header('Location:' . $s);
                    }
                    continue;
                }
            }

            return $this->readRouteStrToParam($s);
        }

        private function __clone()
        {
            // TODO: Implement __clone() method.
        }

        private static $instance;

        public static function init($route_config): ?Route
        {
            if(self::$state == 1){
                return null;
            }

            if(!self::$instance instanceof self){
                self::$instance = new self($route_config);
            }

            return self::$instance;
        }

        private function readRouteStrToParam($s){
            self::$_GET = $_GET;
            self::$_POST = $_POST;

            $routeParams = explode('/', $s);

            foreach ($routeParams as $index => $vo){
                if($vo != ''){
                    if(preg_match("/[a-zA-Z0-9]+-[a-zA-Z0-9]/", $vo)){
                        $param = explode('-', $vo);
                        // 设置参数
                        self::$_GET[htmlentities($param[0])] = $param[1] ? htmlentities($param[1]) : null;
                        $_GET[htmlentities($param[0])] = $param[1] ? htmlentities($param[1]) : null;
                        continue;
                    }else if(is_dir(getcwd() . _DS_ .'platform' . _DS_ . $vo)){
                        self::$platform = $vo;
                        continue;
                    }else if(self::$platform != "" && self::$module == ""){
                        self::$module = $vo;
                        continue;
                    }else if(self::$module != "" && self::$action == ""){
                        self::$action = $vo;
                        continue;
                    }else if(self::$action != ""){
                        $param = explode('-', $vo);
                        if(count($param) == 2){
                            // 设置参数
                            self::$_GET[htmlentities($param[0])] = $param[1] ? htmlentities($param[1]) : null;
                            $_GET[htmlentities($param[0])] = $param[1] ? htmlentities($param[1]) : null;
                            continue;
                        }
                    }
                }
            }

            if(self::$module == "")
                self::$module = "index";

            if(self::$action == "")
                self::$action = "index";

            return self::$instance;
        }

        private function SafeFilter (&$arr): void
        {
            $ra = array('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/','/<\/?script .*?>/','/javascript/','/vbscript/','/expression/','/applet/'
            ,'/meta/','/xml/','/blink/','/link=/','/style=/','/embed/','/object/','/frame/','/layer/','/title/','/bgsound/'
            ,'/base/','/onload/','/onunload/','/onchange/','/onsubmit/','/onreset/','/onselect/','/onblur/','/onfocus/',
                '/onabort/','/onkeydown/','/onkeypress/','/onkeyup/','/onclick/','/ondblclick/','/onmousedown/','/onmousemove/'
            ,'/onmouseout/','/onmouseover/','/onmouseup/','/onunload/');

            if (is_array($arr))
            {
                foreach ($arr as $key => $value)
                {
                    if (!is_array($value))
                    {
                        if (!function_exists('magic_quotes_runtime'))  //不对magic_quotes_gpc转义过的字符使用addslashes(),避免双重转义。
                        {
                            $value  = addslashes($value); //给单引号（'）、双引号（"）、反斜线（\）与 NUL（NULL 字符）
                            #加上反斜线转义
                        }
                        $value       = preg_replace($ra,'',$value);     //删除非打印字符，粗暴式过滤xss可疑字符串
                        $arr[$key]     = trim(htmlentities($value)); //去除 HTML 和 PHP 标记并转换为 HTML 实体
                    }

                    else
                    {
                        $this->SafeFilter($arr[$key]);
                    }
                }
            }
        }
    }
}
