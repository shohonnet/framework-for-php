<?php

namespace System\Core {

    final class TemplateFactory
    {

        private string $tplFileDir = "";
        private string $compileFileDir = "";
        private string $tplContent = "";
        private string $dirSep = '/';
        private array $config = [];

        private function __construct($tplFileDir,$compileFileDir)
        {
            $this->tplFileDir = $tplFileDir;
            $this->compileFileDir = $compileFileDir;
            $this->config = Config::init()->__get('template');

            if(!file_exists($this->tplFileDir)){
                Debug::error('模板文件不存在', ['模板文件：' . $this->tplFileDir]);
            }

            $this->tplContent = file_get_contents($this->tplFileDir);

            $this->compile_output_tag();
            $this->compile_foreach_tag();
            $this->compile_elseif_tag();
            $this->compile_if_tag();
            $this->compile_include_tag();
            $this->compile_import_tag();
            $this->compile_install_data_tag();

            file_put_contents($this->compileFileDir, $this->tplContent);
        }

        public static function compile($tplFileDir,$compileFileDir): void
        {
            new self($tplFileDir,$compileFileDir);
        }

        private function r(): float
        {
            return round(1000, 9999);
        }

        public function compile_install_data_tag(): void
        {
            $pattern = "/<!--\s*model:(.*)\sname:(.*)\swhere:(.*)\sorder:(.*)\slimit:(.*)\s*-->/";

            if (preg_match($pattern, $this->tplContent)) {
                $this->tplContent = preg_replace_callback($pattern, function ($m) {

                    $_model = trim(ucfirst($m[1]));
                    $_model_asName = trim(ucfirst($m[1])) . 'Model' . rand(1000, 9999);
                    $_variable_name = '$' . trim($m[2]);
                    $_where = trim($m[3]);
                    $_order = trim($m[4]);
                    $_limit = trim($m[5]);

                    $_Platform = Route::$platform;

                    if(!file_exists(getcwd() . _DS_ . 'platform' . _DS_ . $_Platform . _DS_ . 'model' . _DS_ . $_model . '.php')){
                        return ;
                    }
                    return <<<PHPCode
<?php
                \$tmp_model = new Platform\\$_Platform\\Model\\$_model();
                $_variable_name = \$tmp_model->tpl_data($_where,$_order,$_limit);
            ?>
PHPCode;
                }, $this->tplContent);
            }
        }

        /**
         * @return void
         */
        private function compile_output_tag(): void
        {
            $pattern = '/\{(([range=[\d,\d|\-\d|\d]+:|time:|date:|datetime:|year:|yearmonth:|count:|monthday:|html:|len:|has:]*)*)?(@?)\$(\w+)((\.\w*)*)\s?\}/';
            if (preg_match($pattern, $this->tplContent)) {
                $this->tplContent = preg_replace_callback($pattern, function ($m) {

                    $tmp_data = [
                        'fns' => explode(":", $m[1]),
                        'startCode' => "<?php echo ",
                        'varSection' => $m['3'] == '@' ? "\$$m[4]" : "\$this->tpl_var['$m[4]']",
                        'endCode' => ' ; ?>'
                    ];

                    $tmp_indexes = $m[5];
                    $tmp_indexes_arr = explode('.', $tmp_indexes);

                    foreach ($tmp_indexes_arr as $val) {
                        if ($val != '') {
                            $tmp_data['varSection'] .= "['" . str_replace('.', '', $val) . "']";
                        }
                    }

                    $fnCode = "";

                    if (count($tmp_data['fns'])) {
                        foreach ($tmp_data['fns'] as $key => $value) {
                            if ($value == 'len') {
                                $fnCode .= 'mb_strlen(';
                                $tmp_data['endCode'] = ')' . $tmp_data['endCode'];
                            } else if ($value == 'count') {
                                $fnCode .= 'count(';
                                $tmp_data['endCode'] = ')' . $tmp_data['endCode'];
                            } else if ($value == 'has') {
                                $fnCode .= 'isset(';
                                $tmp_data['endCode'] = ')' . $tmp_data['endCode'];
                            } else if ($value == 'html') {
                                $fnCode .= 'htmlspecialchars_decode(';
                                $tmp_data['endCode'] = ')' . $tmp_data['endCode'];
                            } else if (preg_match('/range=[\d,\d|\-\d|\d]+/', $value)) {
                                $rangeStr = explode('=', $value);
                                $rangeStr = explode(',', $rangeStr[1]);
                                if (count($rangeStr) == 1) {
                                    $fnCode .= 'mb_substr(';

                                    $tmp_data['endCode'] = ' , 0 ,' . $rangeStr[0] . ')' . " . \$this->addEllipsis(" . $tmp_data['varSection'] . ',' . $rangeStr[0] . ')' . $tmp_data['endCode'];
                                } else if (count($rangeStr) == 2) {
                                    $fnCode .= 'mb_substr(';
                                    $tmp_data['endCode'] = ' , ' . $rangeStr[0] . ' , ' . $rangeStr[1] . ')' . " . \$this->addEllipsis(" . $tmp_data['varSection'] . ',' . $rangeStr[1] . ',' . $rangeStr[0] . ')' . $tmp_data['endCode'];
                                }
                            } else if (preg_match('/[time|date|datetime|year|yearmonth|monthday]+/', $value)) {
                                $type = $value;

                                if ($type == 'time') {
                                    $fnCode .= "date('H:i',";
                                } else if ($type == 'date') {
                                    $fnCode .= "date('Y-m-d',";
                                } else if ($type == 'datetime') {
                                    $fnCode .= "date('Y-m-d H:i:s',";
                                } else if ($type == 'year') {
                                    $fnCode .= "date('Y',";
                                } else if ($type == 'yearmonth') {
                                    $fnCode .= "date('Y-m',";
                                } else if ($type == 'monthday') {
                                    $fnCode .= "date('m-d',";
                                }

                                $tmp_data['endCode'] = ')' . $tmp_data['endCode'];
                            } else {
                                if ($value != '') {
                                    Debug::error('模版错误！', [
                                        '错误信息：使用了不存在的修饰器。',
                                        '模版：' . $this->tplFileDir
                                    ]);
                                }
                            }
                        }
                    }

                    return $tmp_data['startCode'] . $fnCode . $tmp_data['varSection'] . $tmp_data['endCode'];
                }, $this->tplContent);
            }
        }

        /**
         * @return void
         */
        private function compile_foreach_tag(): void
        {
            $pattern = "/<!--\s*loop:(@)?(.*)\((.*?),(.*?)\)\s*-->/";

            if (preg_match($pattern, $this->tplContent)) {

                $this->tplContent = preg_replace_callback($pattern, function ($m) {

                    $outVariable = ( $m[1] == "@" ? true : false );
                    $loopData = explode('.', $m[2]);
                    $loopDataStr = "";
                    $key = $m[3];
                    $item = $m[4];

                    if(!$outVariable){
                        $loopData[0] = str_replace('$', '', $loopData[0]);
                        $loopDataStr .= "\$this->tpl_var['" . $loopData[0] . "']";
                    }else{
                        $loopDataStr .= $loopData[0];
                    }

                    for ($i = 1; $i < count($loopData); $i++) {
                        $loopDataStr .= "['" . $loopData[$i] . "']";
                    }

                    $returnCode = '<?php foreach(' . $loopDataStr . ' as ' . $key . ' => ' . $item . '){ ?>';

                    return $returnCode;
                }, $this->tplContent);
            }

            $this->tplContent = preg_replace("/<!--\s*\/loop\s*-->/", '<?php } ?>', $this->tplContent);
        }

        /**
         * @return void
         */
        private function compile_if_tag(): void
        {
            $pattern = "/<!--\s*if:\((.*?)\)\s*-->/i";
            if (preg_match($pattern, $this->tplContent)) {

                $this->tplContent = preg_replace_callback($pattern, function ($m) {
                    $tj = trim($m[1]);  // 条件
                    $tj_pattern = "/(@?)[\$](\w+)(([\.]\w+)*)/";
                    $tj = preg_replace_callback($tj_pattern, function($tj_m) {
                        $ret = '';
                        if($tj_m[1] == ''){
                            $ret .= "\$this->tpl_var['" . $tj_m[2] . "']";
                        }else{
                            $ret .= "\$" . $tj_m[2];
                        }

                        if(!empty($tj_m[3])){
                            $ch_index = substr($tj_m[3], 1);
                            $ch_index = '[\'' . str_replace('.', '\'][\'', $ch_index) . '\']';
                            $ret .= $ch_index;
                        }

                        return $ret;
                    }, $tj);

                    return "<?php if(" . $tj . "){ ?>";
                }, $this->tplContent);

            }

            $this->tplContent = preg_replace("/<!--\s*\/if\s*-->/", '<?php } ?>', $this->tplContent);


            // 行内模式
            $pattern = "/\{if:(.*?){1}\}/i";
            if (preg_match($pattern, $this->tplContent)) {

                $this->tplContent = preg_replace_callback($pattern, function ($m) {
                    $tj = trim($m[1]);  // 条件
                    $tj_pattern = "/(@?)[\$](\w+)(([\.]\w+)*)/";
                    $tj = preg_replace_callback($tj_pattern, function($tj_m) {
                        $ret = '';
                        if($tj_m[1] == ''){
                            $ret .= "\$this->tpl_var['" . $tj_m[2] . "']";
                        }else{
                            $ret .= "\$" . $tj_m[2];
                        }

                        if(!empty($tj_m[3])){
                            $ch_index = substr($tj_m[3], 1);
                            $ch_index = '[\'' . str_replace('.', '\'][\'', $ch_index) . '\']';
                            $ret .= $ch_index;
                        }

                        return $ret;
                    }, $tj);

                    return "<?php if(" . $tj . "){ ?>";
                }, $this->tplContent);

            }

            $this->tplContent = preg_replace("/\{\/if\}/", '<?php } ?>', $this->tplContent);
        }

        /**
         * @return void
         */
        private function compile_elseif_tag(): void
        {
            $pattern = "/<!--\s*else if:\((.*?)\)\s*-->/i";
            if (preg_match($pattern, $this->tplContent)) {

                $this->tplContent = preg_replace_callback($pattern, function ($m) {
                    $tj = trim($m[1]);  // 条件
                    $tj_pattern = "/(@?)[\$](\w+)(([\.]\w+)*)/";
                    $tj = preg_replace_callback($tj_pattern, function($tj_m) {
                        $ret = '';
                        if($tj_m[1] == ''){
                            $ret .= "\$this->tpl_var['" . $tj_m[2] . "']";
                        }else{
                            $ret .= "\$" . $tj_m[2];
                        }

                        if(!empty($tj_m[3])){
                            $ch_index = substr($tj_m[3], 1);
                            $ch_index = '[\'' . str_replace('.', '\'][\'', $ch_index) . '\']';
                            $ret .= $ch_index;
                        }

                        return $ret;
                    }, $tj);

                    return "<?php } else if(" . $tj . "){ ?>";
                }, $this->tplContent);

            }

            $this->tplContent = preg_replace("/<!--\s*else\s*-->/", '<?php } else { ?>', $this->tplContent);

            // 行内模式
            $pattern = "/\{else if:(.*?){1}\}/i";
            if (preg_match($pattern, $this->tplContent)) {

                $this->tplContent = preg_replace_callback($pattern, function ($m) {
                    $tj = trim($m[1]);  // 条件
                    $tj_pattern = "/(@?)[\$](\w+)(([\.]\w+)*)/";
                    $tj = preg_replace_callback($tj_pattern, function($tj_m) {
                        $ret = '';
                        if($tj_m[1] == ''){
                            $ret .= "\$this->tpl_var['" . $tj_m[2] . "']";
                        }else{
                            $ret .= "\$" . $tj_m[2];
                        }

                        if(!empty($tj_m[3])){
                            $ch_index = substr($tj_m[3], 1);
                            $ch_index = '[\'' . str_replace('.', '\'][\'', $ch_index) . '\']';
                            $ret .= $ch_index;
                        }

                        return $ret;
                    }, $tj);

                    return "<?php } else if(" . $tj . "){ ?>";
                }, $this->tplContent);

            }

            $this->tplContent = preg_replace("/{else}/", '<?php } else { ?>', $this->tplContent);
        }

        /**
         * @return void
         */
        private function compile_include_tag(): void
        {
            $pattern = '/\<!--include:\s?(.*)\.' . 'html' . '\s*-->/';
            if (preg_match($pattern,  $this->tplContent)) {
                $this->tplContent = preg_replace_callback($pattern, function ($m) {
                    $commonTplFileDir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'themes' . $this->dirSep . $this->config['theme'] . $this->dirSep . $this->config['commonTemplateFolderName'];
                    $commonCompileFileDir = getcwd() . $this->dirSep . 'platform' . $this->dirSep . Route::$platform . $this->dirSep . 'compile' . $this->dirSep . $this->config['theme'] . $this->dirSep . $this->config['commonTemplateFolderName'];
                    self::compile($commonTplFileDir . $this->dirSep . $m[1] . $this->config['templateFileExtension'],$commonCompileFileDir . $this->dirSep . md5(Route::$platform . $m[1]) . $this->config['commonCompileFileExtension']);
                    return "<?php \$this->display_common('" . $m[1] . "');?>";

                }, $this->tplContent);
            }
        }

        /**
         * @return void
         */
        private function compile_import_tag(): void
        {
            $pattern = '/\<!--import:\s?(.*)\.' . 'vue' . '\s*-->/';
            if (preg_match($pattern,  $this->tplContent)) {
                $this->tplContent = preg_replace_callback($pattern, function ($m) {
                    return "<?php \$this->display_import('" . $m[1] . "');?>";

                }, $this->tplContent);
            }
        }

    }

}