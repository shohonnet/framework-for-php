<?php

namespace System\Tool {

    use QRcode;

    /**
     * QRCode
     */
    final class Excel{
        public static function init(): PHPExcel
        {
            include_once(getcwd() . '/plugin/' . 'phpexcel/phpexcel.php');
            return new PHPExcel();
        }
    }

}