<?php

namespace System\Tool {
    
    use system\core\Debug;

	/**
	 * File 
	 */
	final class File
	{
		
		public static function getFileContent($file_dir)
		{
			if(!file_exists($file_dir)){
                Debug::error(__CLASS__ . ' Error：文件不存在', ['文件地址：' . $file_dir], false, true);
            }

			return file_get_contents($file_dir);
		}

	}

}