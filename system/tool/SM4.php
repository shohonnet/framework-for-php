<?php

namespace System\Tool {

    use System\Core\Config;
    use System\Core\Tool;

	class SM4 extends Tool {
        /**
         * @param string $str 明文
         * @param string $cipher_algo 加密方式
         * @return string|bool
         * @author: Administrator
         * @Time: 2022/11/27 3:33
         * @Desc: 加密
         */
        public static function encrypt(string $str, string $cipher_algo = '') : string|bool
        {
            $config = Config::init()->__get('sm4');;
            if(empty($cipher_algo))
                $cipher_algo = $config['cipher_algo'];

            $cipher_algo = strtolower($cipher_algo);

            // 检测加密方式是否支持
            if(!in_array($cipher_algo, openssl_get_cipher_methods()))
                trigger_error('当前环境暂不支持' . $cipher_algo, E_USER_ERROR);

            return openssl_encrypt(
                data: $str,
                cipher_algo: $cipher_algo,
                passphrase: $config['key'],
                options: \OPENSSL_ENCODING_DER,
                iv: $config['iv']
            );
        }

        public static function decrypt(string $str, string $cipher_algo = '') : string|bool
        {
            $config = Config::init()->__get('sm4');;
            if(empty($cipher_algo))
                $cipher_algo = $config['cipher_algo'];

            $cipher_algo = strtolower($cipher_algo);

            // 检测加密方式是否支持
            if(!in_array($cipher_algo, openssl_get_cipher_methods()))
                trigger_error('当前环境暂不支持' . $cipher_algo, E_USER_ERROR);

            return openssl_decrypt(
                data: $str,
                cipher_algo: $cipher_algo,
                passphrase: $config['key'],
                options: \OPENSSL_ENCODING_DER,
                iv: $config['iv']
            );
        }
    }
}