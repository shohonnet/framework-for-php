<?php

namespace System\Tool {

    use Platform\Common\Model\Config;

    class Applet {

        private string $appsecret;
        private string $appid;
        private mixed $accessToken;

        public function __construct(string $appid, string $appsecret)
		{

            $this->appid = $appid;
            $this->appsecret = $appsecret;

			$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET';
			$url = str_replace('APPID', $this->appid, $url);
			$url = str_replace('APPSECRET', $this->appsecret, $url);

			$settingObj = new Config();
			$data = $settingObj->get('Applet_accessToken');
			if(!empty($data) && $data['updatetime'] > (time() - 7000)){
				$json = json_decode($data['value'], true);
				$this->accessToken = $json['access_token'];
			}else{
				$res = Http::doGet($url);
				$resJson = json_decode($res, true);
				if(empty($resJson['errcode'])){
					if($settingObj->set('Applet_accessToken', $res)){
						$this->accessToken = $resJson['access_token'];
					}
				}
			}
		}

        /**
         * 授权获取用户手机号
         * @param  [code] js_code
         * @return mixed [type]
         */
		public function getPhoneNumber($code): mixed
        {

			$url = 'https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=ACCESS_TOKEN'; // 请求参数
			$url = str_replace('ACCESS_TOKEN', $this->accessToken, $url);
			$res = $this->curlPost($url, ['code' => $code]);
            return json_decode($res, true);
		}

        /**
         * @param $code
         * @return mixed
         */
		public function getOpenid($code): mixed
        {
			$url = 'https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=APPSECRET&js_code='.$code.'&grant_type=authorization_code';
			$url = str_replace('APPID', $this->appid, $url);
			$url = str_replace('APPSECRET', $this->appsecret, $url);

			$res = $this->curlPost($url);
			$resJson = json_decode($res, true);
			return $resJson;
		}

		public function createPicCode($scene, $page): bool|string
        {
			$url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=ACCESS_TOKEN';
			$url = str_replace('ACCESS_TOKEN', $this->accessToken, $url);
			$res = $this->curlPost($url, [
				'scene' => $scene,
				'env_version' => 'release', // release 正式版    trial 体验版
				'check_path' => false,
				'page' => $page,
				'width' => 1280
			]);
			return $res;
		}

		/**
		 * 发送 curl post 请求
		 * @param  [string]
		 * @param  [array]
		 * @return [array]
		 */
		private function curlPost($url, $data = false): bool|string
        {
			$ch = curl_init();
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data ? json_encode($data) : "");
	        curl_setopt($ch, CURLOPT_URL, $url);
	        if (str_contains($url, 'https')) {
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 对认证证书来源的检查
	            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // 从证书中检查SSL加密算法是否存在
	        }
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 10); // 模拟的header头
	        $result = curl_exec($ch);

	        curl_close($ch);

	        return $result;
		}

	}
}