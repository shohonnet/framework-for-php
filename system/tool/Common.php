<?php

namespace System\Tool {

    final class Common
    {
        public static function inWeChat(): bool
        {
            if (str_contains($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger'))
                return true;
            return false;
        }

        public static function inApp($str): bool
        {
            if (str_contains(strtolower($_SERVER['HTTP_USER_AGENT']), $str))
                return true;
            return false;
        }

        // 用来还原字符串和字符串数组，把已经转义的字符还原回来
        public static function out($data)
        {
            if (is_string($data)) {
                return $data = stripslashes($data);
            } else if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $data[$key] = out($value);
                }
                return $data;
            } else {
                return $data;
            }
        }

        // 文本输出
        public static function text_out($str): string
        {
            $str = str_replace("&nbsp;", " ", $str);
            $str = str_replace("<br>", "\n", $str);
            return stripslashes($str);
        }

        /**
         * 无格式html代码输出
         *
         * @param string $str
         * @return mixed
         */
        public static function html_clean(string $str): mixed
        {
            if (function_exists('htmlspecialchars_decode')) {
                $str = htmlspecialchars_decode($str);
            } else {
                $str = html_entity_decode($str);
            }
            $content = stripslashes($str);

            $content = preg_replace("/<div[^>]*>/i", "", $content);
            $content = preg_replace("/<\/div>/i", "", $content);
            $content = preg_replace("/<strong[^>]*>/i", "<h3>", $content);
            $content = preg_replace("/<\/strong>/i", "</h3>", $content);

            $content = preg_replace("/<!--[^>]*-->/i", "", $content); // 注释内容

            $content = preg_replace("/style=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/class=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/id=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/lang=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/width=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/height=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/border=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace("/face=.+?['|\']/i", "", $content); // 去除样式
            $content = preg_replace('/^[(\xc2\xa0)|\s]+/', '', $content);
            $content = preg_replace('/　/', '', $content);
            $content = preg_replace('/&emsp;/', '', $content);
            // 去除样式 只允许小写 正则匹配没有带 i 参数

            return preg_replace("/face=.+?['|\']/", "", $content);
        }

        // 获取客户端IP地址
        public static function get_client_ip()
        {
            if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
                $ip = getenv("HTTP_CLIENT_IP");
            } else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
                $ip = getenv("HTTP_X_FORWARDED_FOR");
            } else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
                $ip = getenv("REMOTE_ADDR");
            } else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) {
                $ip = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip = "unknown";
            }
            if (preg_match('#^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$#', $ip)) {
                $ip_array = explode('.', $ip);
                if ($ip_array[0] <= 255 && $ip_array[1] <= 255 && $ip_array[2] <= 255 && $ip_array[3] <= 255) {
                    return $ip;
                }
            }
            return "unknown";
        }

        // 获取当前页面地址
        public static function getCurPageURL(): string
        {
            $pageURL = 'http';
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }

        // 获取IP所在地
        public static function getIPLoc($queryIP)
        {
            $url = 'http://ip.qq.com/cgi-bin/searchip?searchip1=' . $queryIP;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_ENCODING, 'gb2312');
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 获取数据返回
            $result = curl_exec($ch);
            $result = mb_convert_encoding($result, "utf-8", "gb2312"); // 编码转换，否则乱码
            curl_close($ch);
            preg_match("@<span>(.*)</span></p>@iU", $result, $ipArray);
            $loc = $ipArray[1];
            return $loc;
        }

        public static function getIPLoc2($queryIP)
        {
            if (empty($queryIP)) {
                $queryIP = get_client_ip();
            }
            $url = 'http://ip.qq.com/cgi-bin/searchip?searchip1=' . $queryIP;
            $result = file_get_contents($url, false);
            $result = mb_convert_encoding($result, "utf-8", "gb2312");
            preg_match("@<span>(.*)</span></p>@iU", $result, $ipArray);
            $loc = $ipArray[1];
            return $loc;
        }

        /**
         * 中文字符串截取
         * @param $str
         * @param $start
         * @param int $length
         * @param string $charset
         * @param bool $suffix
         * @return string|void
         */
        public static function msubstr($str, $start = 0, int $length = 0, string $charset = "utf-8", bool $suffix = true)
        {
            if (empty($str)) {
                return;
            }
            $sourcestr = $str;
            $cutlength = $length;
            $returnstr = '';
            $i = 0;
            $n = 0.0;
            $str_length = strlen($sourcestr); // 字符串的字节数
            while (($n < $cutlength) and ($i < $str_length)) {
                $temp_str = substr($sourcestr, $i, 1);
                $ascnum = ord($temp_str);
                if ($ascnum >= 252) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 6);
                    $i = $i + 6;
                    $n ++;
                } elseif ($ascnum >= 248) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 5);
                    $i = $i + 5;
                    $n ++;
                } elseif ($ascnum >= 240) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 4);
                    $i = $i + 4;
                    $n ++;
                } elseif ($ascnum >= 224) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 3);
                    $i = $i + 3;
                    $n ++;
                } elseif ($ascnum >= 192) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 2);
                    $i = $i + 2;
                    $n ++;
                } elseif ($ascnum >= 65 and $ascnum <= 90 and $ascnum != 73) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 1);
                    $i = $i + 1;
                    $n ++;
                } elseif (! (array_search($ascnum, array(
                        37,
                        38,
                        64,
                        109,
                        119
                    )) === FALSE)) {
                    $returnstr = $returnstr . substr($sourcestr, $i, 1);
                    $i = $i + 1;
                    $n ++;
                } else {
                    $returnstr = $returnstr . substr($sourcestr, $i, 1);
                    $i = $i + 1;
                    $n = $n + 0.5;
                }
            }
            if ($i < $str_length) {
                $returnstr = $returnstr . '...';
            }
            return $returnstr;
        }

        // 检查字符串是否是UTF8编码,是返回true,否则返回false
        public static function is_utf8($string): bool
        {
            if (! empty($string)) {
                $ret = json_encode(array(
                    'code' => $string
                ));
                if ($ret == '{"code":null}') {
                    return false;
                }
            }
            return true;
        }

        // 自动转换字符集 支持数组转换
        public static function auto_charset($fContents, $from = 'gbk', $to = 'utf-8')
        {
            $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
            $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
            if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && ! is_string($fContents))) {
                // 如果编码相同或者非字符串标量则不转换
                return $fContents;
            }
            if (is_string($fContents)) {
                if (function_exists('mb_convert_encoding')) {
                    return mb_convert_encoding($fContents, $to, $from);
                } elseif (function_exists('iconv')) {
                    return iconv($from, $to, $fContents);
                } else {
                    return $fContents;
                }
            } elseif (is_array($fContents)) {
                foreach ($fContents as $key => $val) {
                    $_key = auto_charset($key, $from, $to);
                    $fContents[$_key] = auto_charset($val, $from, $to);
                    if ($key != $_key)
                        unset($fContents[$key]);
                }
                return $fContents;
            } else {
                return $fContents;
            }
        }

        // 浏览器友好的变量输出
        public static function dump($var, $exit = false)
        {
            $output = print_r($var, true);
            $output = "<pre>" . htmlspecialchars($output, ENT_QUOTES) . "</pre>";
            echo $output;
            if ($exit)
                exit();
        }

        // 获取微秒时间，常用于计算程序的运行时间
        public static function utime()
        {
            list ($usec, $sec) = explode(" ", microtime());
            return ((float) $usec + (float) $sec);
        }

        // 生成唯一的值
        public static function cp_uniqid()
        {
            return md5(uniqid(rand(), true));
        }

        public static function uuid()
        {
            $charid = strtoupper(md5(uniqid(mt_rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = // chr(123).// "{"
                substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
            // .chr(125);// "}"
            return $uuid;
        }

        // 加密函数，可用cp_decode()函数解密，$data：待加密的字符串或数组；$key：密钥；$expire 过期时间
        public static function cp_encode($data, $key = '', $expire = 0)
        {
            $string = serialize($data);
            $ckey_length = 4;
            $key = md5($key);
            $keya = md5(substr($key, 0, 16));
            $keyb = md5(substr($key, 16, 16));
            $keyc = substr(md5(microtime()), - $ckey_length);

            $cryptkey = $keya . md5($keya . $keyc);
            $key_length = strlen($cryptkey);

            $string = sprintf('%010d', $expire ? $expire + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
            $string_length = strlen($string);
            $result = '';
            $box = range(0, 255);

            $rndkey = array();
            for ($i = 0; $i <= 255; $i ++) {
                $rndkey[$i] = ord($cryptkey[$i % $key_length]);
            }

            for ($j = $i = 0; $i < 256; $i ++) {
                $j = ($j + $box[$i] + $rndkey[$i]) % 256;
                $tmp = $box[$i];
                $box[$i] = $box[$j];
                $box[$j] = $tmp;
            }

            for ($a = $j = $i = 0; $i < $string_length; $i ++) {
                $a = ($a + 1) % 256;
                $j = ($j + $box[$a]) % 256;
                $tmp = $box[$a];
                $box[$a] = $box[$j];
                $box[$j] = $tmp;
                $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
            }
            return $keyc . str_replace('=', '', base64_encode($result));
        }

        // cp_encode之后的解密函数，$string待解密的字符串，$key，密钥
        public static function cp_decode($string, $key = '')
        {
            $ckey_length = 4;
            $key = md5($key);
            $keya = md5(substr($key, 0, 16));
            $keyb = md5(substr($key, 16, 16));
            $keyc = substr($string, 0, $ckey_length);

            $cryptkey = $keya . md5($keya . $keyc);
            $key_length = strlen($cryptkey);

            $string = base64_decode(substr($string, $ckey_length));
            $string_length = strlen($string);

            $result = '';
            $box = range(0, 255);

            $rndkey = array();
            for ($i = 0; $i <= 255; $i ++) {
                $rndkey[$i] = ord($cryptkey[$i % $key_length]);
            }

            for ($j = $i = 0; $i < 256; $i ++) {
                $j = ($j + $box[$i] + $rndkey[$i]) % 256;
                $tmp = $box[$i];
                $box[$i] = $box[$j];
                $box[$j] = $tmp;
            }

            for ($a = $j = $i = 0; $i < $string_length; $i ++) {
                $a = ($a + 1) % 256;
                $j = ($j + $box[$a]) % 256;
                $tmp = $box[$a];
                $box[$a] = $box[$j];
                $box[$j] = $tmp;
                $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
            }
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
                return unserialize(substr($result, 26));
            } else {
                return '';
            }
        }

        // 遍历删除目录和目录下所有文件
        public static function del_dir($dir)
        {
            if (! is_dir($dir)) {
                return false;
            }
            $handle = opendir($dir);
            while (($file = readdir($handle)) !== false) {
                if ($file != "." && $file != "..") {
                    is_dir("$dir/$file") ? del_dir("$dir/$file") : @unlink("$dir/$file");
                }
            }
            if (readdir($handle) == false) {
                closedir($handle);
                @rmdir($dir);
            }
        }

        // POST表单处理函数,$post_array:POST的数据,$null_value:是否删除空表单,$delete_value:删除指定表单
        public static function postinput($post_array, $null_value = null, $delete_value = array())
        {
            // 清除值为空或者为0的元素
            if ($null_value) {
                foreach ($post_array as $key => $value) {
                    $value = in($value);
                    if ($value == '') {
                        unset($post_array[$key]);
                    }
                }
            }
            // 清除不需要的元素
            $default_value = array(
                'action',
                'button',
                'fid',
                'submit'
            );
            $clear_array = array_merge($default_value, $delete_value);
            foreach ($post_array as $key => $value) {
                if (in_array($key, $clear_array)) {
                    unset($post_array[$key]);
                }
            }
            return $post_array;
        }

        // 复制目录
        public static function copy_dir($sourceDir, $aimDir)
        {
            $succeed = true;
            if (! file_exists($aimDir)) {
                if (! mkdir($aimDir, 0757)) {
                    return false;
                }
            }
            $objDir = opendir($sourceDir);
            while (false !== ($fileName = readdir($objDir))) {
                if (($fileName != ".") && ($fileName != "..")) {
                    if (! is_dir("$sourceDir/$fileName")) {
                        if (! copy("$sourceDir/$fileName", "$aimDir/$fileName")) {
                            $succeed = false;
                            break;
                        }
                    } else {
                        copy_dir("$sourceDir/$fileName", "$aimDir/$fileName");
                    }
                }
            }
            closedir($objDir);
            return $succeed;
        }

        // 判断ajax提交
        public static function is_ajax()
        {
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
                return true;
            if (isset($_POST['ajax']) || isset($_GET['ajax']))
                return true;
            return false;
        }

        public static function isMobile()
        {
            // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
            if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
                return true;
            }
            // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
            if (isset($_SERVER['HTTP_VIA'])) {
                // 找不到为flase,否则为true
                return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
            }
            // 脑残法，判断手机发送的客户端标志,兼容性有待提高。其中'MicroMessenger'是电脑微信
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                $clientkeywords = array(
                    'nokia',
                    'sony',
                    'ericsson',
                    'mot',
                    'samsung',
                    'htc',
                    'sgh',
                    'lg',
                    'sharp',
                    'sie-',
                    'philips',
                    'panasonic',
                    'alcatel',
                    'lenovo',
                    'iphone',
                    'ipod',
                    'blackberry',
                    'meizu',
                    'android',
                    'netfront',
                    'symbian',
                    'ucweb',
                    'windowsce',
                    'palm',
                    'operamini',
                    'operamobi',
                    'openwave',
                    'nexusone',
                    'cldc',
                    'midp',
                    'wap',
                    'mobile',
                    'MicroMessenger'
                );
                // 从HTTP_USER_AGENT中查找手机浏览器的关键字
                if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                    return true;
                }
            }
            // 协议法，因为有可能不准确，放到最后判断
            if (isset($_SERVER['HTTP_ACCEPT'])) {
                // 如果只支持wml并且不支持html那一定是移动设备
                // 如果支持wml和html但是wml在html之前则是移动设备
                if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                    return true;
                }
            }

            return false;
        }

        /**
         * 获取当前毫秒时间
         *
         * @return [type] [description]
         */
        public static function getMmsectime()
        {
            list ($msec, $sec) = explode(' ', microtime());
            $msectime = (float) sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
            return $msectime;
        }

        /**
         * php毫秒转时间
         *
         * @param [type] $time
         *            [description]
         * @return [type] [description]
         */
        public static function msecdate($time)
        {
            $tag = 'Y-m-d H:i:s';
            $a = substr($time, 0, 10);
            $b = substr($time, 10);
            $date = date($tag, $a) . '.' . $b;
            return $date;
        }
    }
}