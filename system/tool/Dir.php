<?php
namespace System\Tool {
    final class Dir {
        /**
         * 获取文件夹下的文件
         * @param string $path
         * @param string|bool $contain
         * @return array
         */
        public static function getFiles(string $path, string|bool $contain = false) : array
        {
            if(empty($path))
                return false;

            if(!is_dir($path)){
                return [];
            }

            $filename = scandir($path);
            $fileList = [];

            foreach($filename as $k=>$v){

                // 跳过两个特殊目录   continue跳出循环

                if($v=="." || $v==".."){continue;}

                //截取文件名，我只需要文件名不需要后缀;然后存入数组。如果你是需要后缀直接$v即可
                if(!empty($contain)){
                    if(!str_contains($v, $contain)){
                        continue;
                    }
                }

                $fileList[] = $v;
            }

            return $fileList;
        }

    }
}
