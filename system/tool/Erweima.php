<?php

namespace System\Tool {

    use QRcode;

    /**
     * QRCode
     */
    final class Erweima{
        public static function init(): QRcode
        {
            include_once(getcwd() . '/plugin/' . 'phpqrcode/phpqrcode.php');
            return new QRcode();
        }
    }

}