<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/19
 * Time: 4:19
 */
namespace System\Tool {

    class Image
    {
        public function png2jpg($srcPathName, $delOri=true)
        {
            $srcFile=$srcPathName;
            $srcFileExt=strtolower(trim(substr(strrchr($srcFile,'.'),1)));
            if($srcFileExt=='png')
            {
                $dstFile = str_replace('.png', '.jpg', $srcPathName);
                $photoSize = GetImageSize($srcFile);
                $pw = $photoSize[0];
                $ph = $photoSize[1];
                $dstImage = ImageCreateTrueColor($pw, $ph);
                imagecolorallocate($dstImage, 255, 255, 255);
                //读取图片
                $srcImage = ImageCreateFromPNG($srcFile);
                //合拼图片
                imagecopyresampled($dstImage, $srcImage, 0, 0, 0, 0, $pw, $ph, $pw, $ph);
                imagejpeg($dstImage, $dstFile, 90);
                if ($delOri)
                {
                    unlink($srcFile);
                }
                imagedestroy($srcImage);
            }
        }

        public function outputImg($url,$width = "",$height = ""){
            $w = 0;
            $h = 0;

            if($width == "" && $height == ""){
                //输出整张图象
                $size = getimagesize($url);
                header('Content-Type: $size["mime"]');
                echo file_get_contents($url);
            }else{
                //输出图像为指定宽高
            }
        }
    }
}