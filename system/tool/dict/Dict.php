<?php

namespace System\Tool\dict {

    use System\Tool\SM4;

    class Dict
    {
        private string $dictDir;
        private string $dictDirectoryDir;

        /**
         * @throws DictException
         */
        private function __construct()
        {
            $this->dictDir = getcwd() . _DS_ . 'data' . _DS_ . 'dict' . _DS_;
            $this->dictDirectoryDir = getcwd() . _DS_ . 'data' . _DS_ . 'dict' . _DS_ . '.dict';
            if(!is_writable ( $this->dictDir )){
                throw new DictException('目录权限不足【无写入权限】');
            }
        }

        private function __clone(): void{}

        private static self|null $instance = null;

        public static function getInstance() : self
        {
            if(!self::$instance instanceof self){
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * @throws DictException
         */
        private function check($dictName): void
        {
            if(!$this->has($dictName)){
                throw new DictException('字典【' . $dictName . '】不存在');
            }
        }

        /**
         * @throws DictException
         */
        public function list(): array
        {
            $list = $this->directory_get();
            foreach($list as $key => $vo){
                $vo['data'] = $this->read($vo['name']);
                $list[$key] = $vo;
            }
            return $list;
        }

        /**
         * @throws DictException
         */
        public function create($dictName, $alias, $content = '') : bool
        {
            $dict_file_name = md5($dictName) . '.dict';
            $dict_file_dir = $this->dictDir . $dict_file_name;

            if(file_exists($dict_file_dir)){
                throw new DictException('字典【' . $dictName . '】已存在');
            }

            if($this->directory_add($dictName, $alias)){
                return file_put_contents($dict_file_dir, $content);
            }
        }

        /**
         * @throws DictException
         */
        public function delete($dictName) : bool
        {
            $dict_file_name = md5($dictName) . '.dict';
            $dict_file_dir = $this->dictDir . $dict_file_name;

            $this->check($dictName);

            if($this->directory_delete($dictName)){
                return unlink($dict_file_dir);
            }else{
                return false;
            }
        }

        public function has($dictName): bool
        {
            $dict_file_name = md5($dictName) . '.dict';
            $dict_file_dir = $this->dictDir . $dict_file_name;

            if(file_exists($dict_file_dir)){
                return true;
            }else{
                return false;
            }
        }

        /**
         * @throws DictException
         */
        public function write(string $dictName, array $content = []) : bool
        {
            $this->check($dictName);

            $dict_file_name = md5($dictName) . '.dict';
            $dict_file_dir = $this->dictDir . $dict_file_name;

            return file_put_contents($dict_file_dir, SM4::encrypt(serialize($content)));
        }

        /**
         * @throws DictException
         */
        public function read(string $dictName) : array
        {
            $this->check($dictName);

            $dict_file_name = md5($dictName) . '.dict';
            $dict_file_dir = $this->dictDir . $dict_file_name;

            return unserialize(SM4::decrypt(file_get_contents($dict_file_dir)));
        }

        /**
         * @throws DictException
         */
        public function query(string $dictName, $field, $value) : array
        {
            $this->check($dictName);

            $dict_data = $this->read($dictName);

            $data = [];
            foreach($dict_data as $v){
                if($v[$field] == $value){
                    $data[] = $v;
                }
            }
            return $data;
        }

        /**
         * @throws DictException
         */
        public function first(string $dictName, $field, $value, string|null $returnField = null) : array
        {
            $this->check($dictName);

            $dict_data = $this->read($dictName);

            foreach($dict_data as $v){
                if($v[$field] == $value){
                    if(!empty($returnField)){
                        return $v[$returnField];
                    }else{
                        return $v;
                    }
                    break;
                }
            }
        }

        private function directory_add($dictName, $alias): bool|int
        {
            $directory = json_decode(file_get_contents($this->dictDirectoryDir), true);
            $directory[md5($dictName)] = ['name' => $dictName, 'alias' => $alias];
            return file_put_contents($this->dictDirectoryDir, json_encode($directory));
        }

        private function directory_delete($dictName): bool|int
        {
            $directory = json_decode(file_get_contents($this->dictDirectoryDir), true);
            foreach ($directory as $key => $vo){
                if($vo['name'] == $dictName){
                    unset($directory[$key]);
                }
            }
            return file_put_contents($this->dictDirectoryDir, json_encode($directory));
        }

        private function directory_get(): array
        {
            return json_decode(file_get_contents($this->dictDirectoryDir), true);
        }
    }
}