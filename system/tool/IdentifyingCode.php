<?php
namespace System\Tool {

	class IdentifyingCode{

		private $im = NULL;
		private $im_w = NULL;
		private $im_h = NULL;
		private $code_len = NULL;
		private $sessionName = NULL;
		private $isShowLine = NULL;
		private $text = NULL;

		/**
		 * [__construct description]
		 * @param [type] $im_w        [宽]
		 * @param [type] $im_h        [高]
		 * @param [type] $len         [长度]
		 * @param [type] $sessionName [session键]
		 */
		public function __construct($im_w,$im_h,$len,$sessionName,$isShowLine = true,$isShowDian = true,$text = null){
			
			if(gettype($im_w) != gettype(0) || gettype($im_h) != gettype(0) || gettype($len) != gettype(0)){
				die("\$im_w与\$im_h必须为整型！");
			}

			$this->im_w = $im_w;
			$this->im_h = $im_h;
			$this->code_len = $len;
			$this->sessionName = $sessionName;
            $this->isShowLine = $isShowLine;
            $this->isShowDian = $isShowDian;
            $this->text = $text;

			if(!isset($_SESSION)){
			    session_start();
			}
		}
		private function showLine(){
            for($i=0;$i<10;$i++){
                $linecolor = imagecolorallocate($this->im,rand(100,150), rand(100,150),rand(100,150));
                imageline($this->im,rand(1,$this->im_w), rand(1,$this->im_h),rand(1,$this->im_w), rand(1,$this->im_h), $linecolor);
            }
        }

		public function showImg(){
			// Set the content-type
			// Create the image
			$this->CreateCanvas();
			// Create some colors
			$white = imagecolorallocate($this->im, 255, 255, 255);
			imagefilledrectangle($this->im, 0, 0, 230, $this->im_h, $white);

			// The text to draw
			$text = $this->text?$this->text:array(1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');

			$vcode = null;
			for($z=0;$z<$this->code_len;$z++){
				$vcode .= $text[rand(0,(count($text)-1))];
			}

			$_SESSION[$this->sessionName] = strtolower($vcode);

			$font =  getcwd() . '/public/font/kumo.ttf';
			if(!file_exists($font)){
				trigger_error( __CLASS__ . '：字体文件不存在.', E_USER_ERROR);
			}

			if ($this->isShowDian) {
				// 划点
	            for($i=0; $i<500; $i++){
	                $pointcolor = imagecolorallocate($this->im, rand(50,200), rand(50,200), rand(50,200));
	                imagesetpixel($this->im, rand(1,$this->im_w), rand(1,$this->im_h), $pointcolor);
	            }
			}

            if($this->isShowLine){
                $this->showLine();
            }

			for($i=0;$i<$this->code_len;$i++){
				$left = 10;
				if($i == 0){
					$left = 20;
				}
				
				imagettftext($this->im, $this->im_h * 0.7, rand(-10,10), (int) (($this->im_w) / $this->code_len * $i), ($this->im_h*0.8), imagecolorallocate($this->im, rand(0,5), rand(0,200), rand(0,5)), $font, $vcode[$i]);
			}

			header('Content-Type: image/png');
			imagepng($this->im);
			imagedestroy($this->im);
		}

		//创建画布
		private function CreateCanvas(){
			$this->im = imagecreatetruecolor($this->im_w, $this->im_h);
		}
	}
}
?>