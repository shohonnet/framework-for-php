<?php

namespace System\Lib {

    use System\Core\Config;
    use System\Lib\Dao\DaoInterface;

    class Dao
    {
        public DaoInterface|null $drive = null;
        public array $config = [];

        /**
         * @throws \ReflectionException
         */
        public function __construct()
        {
            $this->config = Config::init()->__get('database');
            $drive_namespace_name = "System\\Lib\\Dao\\" . $this->config['drive'];
            $refClass = new \ReflectionClass($drive_namespace_name);
            $refMethod = $refClass->getMethod('init');
            $this->drive = $refMethod->invoke(null, $this->config);
        }

        public function setTable(string $tablename, string $alias = 'A'): self
        {
            $this->drive->setTable($tablename, $alias);
            return $this;
        }

        public function setWhere(array ...$whereParams): self
        {
            $this->drive->setWhere(...$whereParams);
            return $this;
        }

        public function setPage(int $page = 1, int $limit = 10): self
        {
            $this->drive->setPage($page, $limit);
            return $this;
        }

        public function setData(array $data): self
        {
            $this->drive->setData($data);
            return $this;
        }

        public function setField(...$fieldList): self
        {
            $this->drive->setField(fieldList: $fieldList);
            return $this;
        }
        public function setExtendTable($tablename, $alias, $relation): self
        {
            $this->drive->setExtendTable($tablename, $alias, $relation);
            return $this;
        }

        public function setOrder(array $order_field = []): self
        {
            $this->drive->setOrder($order_field);
            return $this;
        }

        public function setGroup(string $group_field): self
        {
            $this->drive->setGroup($group_field);
            return $this;
        }

        public function query(): array
        {
            return $this->drive->query();
        }

        public function first(): array
        {
            return $this->drive->first();
        }

        public function count(): int
        {
            return $this->drive->count();
        }

            public function update() : int
        {
            return $this->drive->update();
        }

        public function insert() : int
        {
            return $this->drive->insert();
        }

        public function delete() : int
        {
            return $this->drive->delete();
        }
    }
}