<?php
namespace System\Lib {

    use system\tool\Randstr;

    final class Upload
    {

        private array $allow_type = [];
        private int $max_size = 0;
        private string $upload_dir;
        public array $files = [];
        private static ?Upload $instance = null;

        private function __construct(array $allow_type = ['image/jpg','image/jpeg','image/png','video/mp4','audio/mp3'], $max_size = 0)
        {
            $this->upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/uploadfile/';
            $this->max_size = $max_size;
            $this->allow_type = $allow_type;
        }

        private function __clone(){}

        public static function getInstance(): ?Upload
        {
            if (! self::$instance instanceof self) {
                self::$instance = new self();
            }
            
            return self::$instance;
        }

        /**
         * 开始接收文件
         */
        public function start_receive(): ?Upload
        {
            $files = $_FILES;
            
            if (is_array($files)) {
                foreach ($files as $key => $info) {
                    // 检测是否为多图
                    if (is_array($info['name'])) {
                        // 多图
                        $this->files[$key] = [];
                        
                        for ($i = 0; $i < count($info['name']); $i ++) {
                            $tmp_info = [
                                'name' => $info['name'][$i],
                                'type' => $info['type'][$i],
                                'tmp_name' => $info['tmp_name'][$i],
                                'error' => $info['error'][$i],
                                'size' => $info['size'][$i]
                            ];
                            
                            $tmp_info['ext_name'] = pathinfo($tmp_info['name'], PATHINFO_EXTENSION);
                            $tmp_info['to_name'] = Randstr::getCode(32);
                            $tmp_info['to_dir'] = date('Ym') . _DS_ . date('d') . _DS_;
                            $tmp_info['state'] = 0; // 0 正常 1文件类型被拒绝 2文件过大
                            $tmp_info['msg'] = '正常';
                            $tmp_info['move_dir'] = $this->upload_dir . $tmp_info['to_dir'] . $tmp_info['to_name'] . '.' . $tmp_info['ext_name'];
                            $tmp_info['web_url'] = $tmp_info['to_dir'] . $tmp_info['to_name'] . '.' . $tmp_info['ext_name'];
                            $tmp_info['web_url'] = '/uploadfile/' . str_replace("\\","/",$tmp_info['web_url']);
                            $tmp_info['move_state'] = 1; // 0已移动 1等待移动 2移动失败
                            $tmp_info['move_msg'] = '待移动';
                            
                            if (! in_array($tmp_info['type'], $this->allow_type)) {
                                $tmp_info['state'] = 1;
                                $tmp_info['msg'] = '文件类型被拒绝';
                            }
                            
                            if ($tmp_info['size'] > $this->max_size) {
                                $tmp_info['state'] = 2;
                                $tmp_info['msg'] = '文件超过限制大小';
                            }
                            
                            $this->files[$key][] = $tmp_info;
                        }
                    } else {
                        $tmp_info = $info;
                        $tmp_info['ext_name'] = pathinfo($tmp_info['name'], PATHINFO_EXTENSION);
                        $tmp_info['to_name'] = Randstr::getCode(32);
                        $tmp_info['to_dir'] = date('Ym') . '/' . date('d') . '/';
                        $tmp_info['state'] = 0; // 0 正常 1文件类型被拒绝 2文件过大 3转移错误
                        $tmp_info['msg'] = '正常';
                        $tmp_info['move_dir'] = $this->upload_dir . $tmp_info['to_dir'] . $tmp_info['to_name'] . '.' . $tmp_info['ext_name'];
                        $tmp_info['web_url'] = $tmp_info['to_dir'] . $tmp_info['to_name'] . '.' . $tmp_info['ext_name'];
                        $tmp_info['web_url'] = '/uploadfile/' . str_replace("\\","/",$tmp_info['web_url']);
                        $tmp_info['move_state'] = 1; // 0已移动 1等待移动 2移动失败
                        $tmp_info['move_msg'] = '待移动';
                        
                        if (! in_array($tmp_info['type'], $this->allow_type)) {
                            $tmp_info['state'] = 1;
                            $tmp_info['msg'] = '文件类型被拒绝';
                        }
                        
                        if ($tmp_info['size'] > $this->max_size) {
                            $tmp_info['state'] = 2;
                            $tmp_info['msg'] = '文件超过限制大小';
                        }
                        
                        // 单图
                        $this->files[$key] = [
                            $tmp_info
                        ];
                    }
                }
            }
            return $this->getInstance();
        }

        public function set_allow_type(array $allow_type): ?Upload
        {
            $this->allow_type = $allow_type;
            return $this->getInstance();
        }

        public function set_max_size(int $max_size): ?Upload
        {
            $this->max_size = $max_size;
            return $this->getInstance();
        }

        public function start_move(): void
        {
            // 转移文件
            foreach ($this->files as $key => $val) {
                for ($i = 0; $i < count($val); $i ++) {
                    if ($val[$i]['state'] != 0) {
                        $this->files[$key][$i]['move_state'] = 2; // 0已移动 1等待移动 2移动失败
                        $this->files[$key][$i]['move_msg'] = '移动失败';
                    } else {
                        // 开始移动
                        // 检测文件夹是否存在
                        if (! is_dir(dirname($val[$i]['move_dir']))) {
                            mkdir(dirname($val[$i]['move_dir']), 0757, true);
                        }
                        
                        if (move_uploaded_file($val[$i]['tmp_name'], $val[$i]['move_dir'])) {
                            $this->files[$key][$i]['move_state'] = 0; // 0已移动 1等待移动 2移动失败
                            $this->files[$key][$i]['move_msg'] = '已移动';
                        } else {
                            $this->files[$key][$i]['move_state'] = 2; // 0已移动 1等待移动 2移动失败
                            $this->files[$key][$i]['move_msg'] = '移动失败';
                        }
                    }
                }
            }
        }
    }
}