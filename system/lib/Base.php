<?php

namespace System\Lib {

    class Base extends Model implements ModelInterface
    {

        protected string $tableName = '';
        protected array $orderField = [];

        public function __construct($tableName = '')
        {
            parent::__construct();
            if(!empty($tableName)){
                $this->tableName = $tableName;
            }

            if(empty($this->tableName)){
                trigger_error("Base model's Tablename property cannot be empty", E_USER_ERROR);
            }
        }

        /**
         * @param int $page
         * @param int $limit
         * @param array $where
         * @return array
         */
        public function list(int $page = 1, int $limit = 10, array $where = []): array
        {
            // TODO: Implement list() method.
            return $this->_DAO->setTable($this->tableName, 'A')->setPage($page, $limit)->setWhere(...$where)->setOrder($this->orderField)->query();
        }

        /**
         * @param array $where
         * @return int
         */
        public function count(array $where = []): int
        {
            // TODO: Implement count() method.
            return $this->_DAO->setTable($this->tableName, 'A')->setWhere(...$where)->count();
        }

        /**
         * @param array $where
         * @return array
         */
        public function info(array $where): array
        {
            // TODO: Implement info() method.
            return $this->_DAO->setTable($this->tableName, 'A')->setWhere(...$where)->first();
        }

        /**
         * @param array $data
         * @return int
         */
        public function insert(array $data): int
        {
            // TODO: Implement info.vue() method.
            return $this->_DAO->setTable($this->tableName, 'A')->setData($data)->insert();
        }

        /**
         * @param array $data
         * @param array $where
         * @return int
         */
        public function update(array $data, array $where): int
        {
            // TODO: Implement update() method.
            return $this->_DAO->setTable($this->tableName, 'A')->setData($data)->setWhere(...$where)->update();
        }

        /**
         * @param array $where
         * @return int
         */
        public function delete(array $where) : int
        {
            // TODO: Implement delete() method.
            return $this->_DAO->setTable($this->tableName, 'A')->setWhere(...$where)->delete();
        }
    }

}