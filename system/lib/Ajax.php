<?php
namespace System\Lib {

    use JetBrains\PhpStorm\NoReturn;
    use system\tool\Common;

    final class Ajax
    {
        private static mixed $instance = null;
        private function __construct() {}
        private function __clone(): void {}
        public static function init() : Ajax
        {
            if(!self::$instance instanceof Ajax){
                self::$instance = new self();
            }
            return self::$instance;
        }

        // 应以默认接口数据结构
        private static array $return_data = [
            'code' => 1,
            'msg' => '',
            'data' => []
        ];

        public static string $fh = '.';

        #[NoReturn] public function show(string $msg, int $code = 1, array $data = [], int|bool $count = NULL, array $othorData = [], array $session_data = []): void
        {
            if (Common::is_ajax() === false) {
                header("HTTP/1.0 404 Not Found");
                header("Status: 404 Not Found");
                exit();
            }

            self::$return_data['code'] = $code;
            self::$return_data['data'] = $data;
            self::$return_data['msg'] = $msg . self::$fh;

            if (! $count) {
                self::$return_data['count'] = count($data);
            } else {
                self::$return_data['count'] = $count;
            }

            if (is_array($othorData) && count($othorData) != 0) {
                self::$return_data['othorData'] = $othorData;
            }

            if (count($session_data) > 0) {
                foreach ($session_data as $key => $value) {
                    $_SESSION[$key] = $value;
                }
            }

            header('Content-Type:application/json');
            die(json_encode(self::$return_data));
        }
    }
}