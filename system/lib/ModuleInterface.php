<?php

namespace System\Lib;

interface ModuleInterface
{
    public function index();
    public function data();
    public function insert();
    public function updata();
    public function delete();
    public function compile_param();
}