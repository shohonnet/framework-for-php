<?php
namespace System\Lib{

    use JetBrains\PhpStorm\NoReturn;
    use system\core\Route;
    use system\core\Template;
    use System\Tool\dict\Dict;

    class Module
    {

        protected Template $tpl;
        protected Ajax $ajax;
        protected Dict $dict;

        public function __construct()
        {
            // 检测当前操作
            $this->tpl = new Template();
            $this->ajax = Ajax::init();
            $this->dict = Dict::getInstance();
            Session::_init();
        }

        /**
         * @name: _getModule
         * @return string
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc:
         */
        protected function _getModule() : string
        {
            return Route::$module;
        }

        /**
         * @name: _getAction
         * @return string
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc:
         */
        protected function _getAction() : string
        {
            return Route::$action;
        }

        /**
         * @name: _getPlatform
         * @return string
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc:
         */
        protected function _getPlatform() : string
        {
            return Route::$platform;
        }

        /**
         * @name: _jump
         * @param $url
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc:
         */
        #[NoReturn] protected function _jump($url): void
        {
            header("Location: $url");
            die();
        }

        /**
         * @name: _jumpAction
         * @param string $action
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc: 当前平台 当前模块下 操作间跳转
         */
        #[NoReturn] protected function _jumpAction(string $action): void
        {
            if(Route::$platform == Route::$default_platform){
                $this->_jump("/" . Route::$module . "/" . $action);
            }else{
                $this->_jump("/" . Route::$platform . "/" . Route::$module . "/" . $action);
            }
        }

        /**
         * @name: _jumpModule
         * @param string $module
         * @param string $action
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc: 当前平台下[模块|操作]间跳转
         */
        #[NoReturn] protected function _jumpModule(string $module, string $action = 'index'): void
        {
            if(Route::$platform == Route::$default_platform) {
                $this->_jump("/" . $module . "/" . $action);
            }else{
                $this->_jump("/" . Route::$platform . "/" . $module . "/" . $action);
            }
        }

        /**
         * @name: _jumpPlatform
         * @param string $platform
         * @param string $module
         * @param string $action
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc: 当前系统下[平台|模块|操作]间跳转
         */
        protected function _jumpPlatform(string $platform, string $module = 'index', string $action = 'index'): void
        {
            if(!empty($platform)){
                if($platform == Route::$default_platform) {
                    $this->_jump("/" . $module . "/" . $action);
                }else{
                    $this->_jump("/" . $platform . "/" . $module . "/" . $action);
                }
            }
        }

        /**
         * @name: _getInputGetData
         * @param $index
         * @param $defaultValue
         * @return mixed
         * @author: Administrator
         * @Time: 2022/11/24 1:52
         * @Desc: 获取get数据
         */
        protected function _getInputGetData($index, $defaultValue = null): mixed
        {
            if(gettype($index) == 'string'){
                if(isset(Route::$_GET[$index])){
                    if(empty(Route::$_GET[$index])){
                        return $defaultValue;
                    }else{
                        return Route::$_GET[$index];
                    }
                }else{
                    return $defaultValue;
                }
            }elseif(gettype($index) == 'array'){
                $tmp = Route::$_GET;
                return $this->extracted($index, $tmp, $defaultValue);
            }else{
                return $defaultValue;
            }
        }

        /**
         * @name: _getInputPostData
         * @param $index
         * @param $defaultValue
         * @return mixed|null
         * @author: Administrator
         * @Time: 2022/11/24 1:53
         * @Desc: 获取post数据
         */
        protected function _getInputPostData($index, $defaultValue = null): mixed
        {
            if(gettype($index) == 'string'){
                if(isset($_POST[$index])){
                    if(empty($_POST[$index])){
                        return $defaultValue;
                    }else{
                        return $_POST[$index];
                    }
                }else{
                    return $defaultValue;
                }
            }elseif(gettype($index) == 'array'){
                $tmp = $_POST;
                return $this->extracted($index, $tmp, $defaultValue);
            }else{
                return $defaultValue;
            }
        }

        /**
         * @name: extracted
         * @param $index
         * @param mixed $tmp
         * @param mixed $defaultValue
         * @return mixed
         * @author: Administrator
         * @Time: 2022/12/3 1:35
         * @Desc:
         */
        private function extracted($index, mixed $tmp, mixed $defaultValue): mixed
        {
            if (count($index)) {
                foreach ($index as $key => $value) {
                    if (isset($tmp[$value])) {
                        if ($key == (count($index) - 1)) {
                            if (!empty($tmp[$value])) {
                                return $tmp[$value];
                            } else {
                                return $defaultValue;
                            }
                        } else {
                            $tmp = $tmp[$value];
                        }
                    } else {
                        return $defaultValue;
                    }
                }
            }
            return $defaultValue;
        }
        /**
         * @name: _404
         * @author: Administrator
         * @Time: 2022/11/24 1:53
         * @Desc: 跳转404
         */
        #[NoReturn] protected function _404(): void
        {
            @header("http/1.1 404 not found");
            @header("status: 404 not found");
            // echo 'echo 404';//直接输出页面错误信息
            exit();
        }

    }
}
