<?php

namespace System\Lib\Dao {

    use PDO;
    use System\Core\Tool;

    /**
     *  统一采用单例模式
     */
    class PDO_MySQL implements DaoInterface
    {
        private PDO|null $_DAO = null;
        private static self|null $instance = null;
        private function __construct(){}
        private function __clone(): void{}

        private array|null $config = [];
        private array $field = [];
        private array $table = [];
        private array $extend_table = [];
        private array $data = [];
        private array $where = [];
        private array $order = [];
        private int $page = 1;
        private int $limit = 10;
        private string|null $group = null;
        private array $sql = [];

        private array $param = [];

        public static function init(array $config) : self
        {
            if(!self::$instance instanceof self){
                self::$instance = new self();
            }
            self::$instance->config = $config;
            self::$instance->connect();
            return self::$instance;
        }

        public function connect() : void
        {
            $this->_DAO = new PDO(
                dsn: 'mysql:host=' . $this->config['host'] . ';dbname=' . $this->config['dbname'],
                username: $this->config['user'],
                password: $this->config['password'],
                options: [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ]
            );
        }

        public function close()
        {
            $this->_DAO = null;
        }

        /**
         * @param string ...$fieldList
         * @return mixed
         */
        public function setField($fieldList): DaoInterface
        {
            $this->field = $fieldList;
            return $this;
        }

        /**
         * @param string $tablename
         * @param string $alias
         * @return DaoInterface
         */
        public function setTable(string $tablename, string $alias = 'A'): DaoInterface
        {
            $this->table = [$tablename, $alias];
            return $this;
        }

        /**
         * @return mixed
         */
        public function setExtendTable($tablename, $alias, $relation): DaoInterface
        {
            $this->extend_table[] = [$tablename, $alias, $relation];
            return $this;
        }

        /**
         * @param array $whereParams
         * @return DaoInterface
         */
        public function setWhere(array ...$whereParams) : DaoInterface
        {
            $where_data = [];
            foreach ($whereParams as $vo){
                if(!empty($vo)){
                    $where_data[] = $vo;
                }
            }
            $this->where = $where_data;
            return $this;
        }

        /**
         * @return mixed
         */
        public function setData(array $data): DaoInterface
        {
            $this->data = $data;
            return $this;
        }

        /**
         * @param string $group_field
         * @return DaoInterface
         */
        public function setGroup(string $group_field): DaoInterface
        {
            $this->group = $this->format_name($group_field);
            return $this;
        }

        /**
         * @return mixed
         */
        public function setOrder(array $order_field = []): DaoInterface
        {
            $this->order = $order_field;
            return $this;
        }

        /**
         * @return mixed
         */
        public function setPage(int $page = 1, int $limit = 10): DaoInterface
        {
            $this->page = $page;
            $this->limit = $limit;
            return $this;
        }

        private function format_name(string $name) : string
        {
            if(str_contains($name, '.')){
                $name_arr = explode('.', $name);
                $new_name = '';
                if(count($name_arr) != 2){
                    user_error('参数错误', E_USER_ERROR);
                }

                for ($i = 0; $i < count($name_arr); $i++){
                    if($name_arr[$i] == '*'){
                        $new_name .= '*';
                        break;
                    }
                    $new_name .= '`'.$name_arr[$i].'`' . (($i == (count($name_arr) - 1)) ? '' : '.');
                }
                return $new_name;
            }elseif (str_contains($name, ')')){
                return $name;
            }else{
                return '`' . $name . '`';
            }
        }

        private function comparison($item): string
        {
            if(gettype($item[1]) == 'array'){
                $sql = count($item[1]) > 1 ? ' ( ' : ' ';
                foreach ($item[1] as $key => $v){
                    if($key == count($item[1])  - 1 ) {
                        $sql .= ' FIND_IN_SET( ? , ' . $this->format_name($item[0]) . ')';
                    }else {
                        $sql .= implode(' ', [
                            ' FIND_IN_SET( ? , ',
                            $this->format_name($item[0]),
                            ')' . ' ' . ($item[2] ?? 'or' . ' ')
                        ]);
                    }
                    $this->param[] = $v;
                }
                $sql .= count($item[1]) > 1 ? ' ) ' : ' ';
                if(!empty($item[3])){
                    $sql .= $item[3] . ' ';
                }
            }else{
                $sql = $item[1];
            }
            return $sql;
        }

        private function reset(): void
        {
            $this->sql = [];
            $this->param = [];
            $this->field = [];
            $this->table = [];
            $this->extend_table = [];
            $this->data = [];
            $this->where = [];
            $this->group = null;
            $this->order = [];
            $this->page = 1;
            $this->limit = 10;
        }

        /**
         * @return array
         */
        public function query(): array
        {
            if(empty($this->table) || count($this->table) != 2)
                user_error('表名错误', E_ERROR);

            $this->sql = [
                'px'            => 'SELECT ',
                'field'         => $this->_getField(),
                'FROM',
                'table'         => $this->_getTable(),
                'extend_table'  => $this->_getExtendTable(),
                'where'         => $this->_getWhere(),
                'group'         => $this->_getGroup(),
                'order'         => $this->_getOrder(),
                'limit'         => $this->_getLimit()
            ];

            # Tool::dump(implode(' ', $this->sql));
            # Tool::dump($this->param);
            # die();

            try {
                $statment = $this->_DAO->prepare(implode(' ', $this->sql));
                $statment->execute($this->param);
                $this->reset();
                return $statment->fetchAll(PDO::FETCH_ASSOC);
            }catch (\PDOException $e){
                die(implode(' ', $this->sql));
            }
        }

        /**
         * @return array
         */
        public function first(): array
        {
            // TODO: Implement first() method.
            $this->page = 1;
            $this->limit = 1;
            $data = $this->query();
            if(\count($data)){
                return $data[0];
            }else{
                return [];
            }
        }

        public function count(): int
        {
            if(empty($this->table) || count($this->table) != 2)
                user_error('表名错误', E_ERROR);

            $this->sql = [
                'px'            => 'SELECT ',
                'field'         => ' count(1) ',
                'FROM',
                'table'         => $this->_getTable(),
                'extend_table'  => $this->_getExtendTable(),
                'where'         => $this->_getWhere(),
                'group'         => $this->_getGroup(),
                'order'         => $this->_getOrder(),
                'limit'         => $this->_getLimit()
            ];

            // die(implode(' ', $this->sql));

            $statment = $this->_DAO->prepare(implode(' ', $this->sql));
            $statment->execute($this->param);
            $this->reset();
            return $statment->fetchAll(PDO::FETCH_ASSOC)[0]['count(1)'];
        }

        /**
         * @return int
         */
        public function update(): int
        {
            if(empty($this->table) || count($this->table) != 2)
                user_error('表名错误', E_USER_ERROR);
            if(empty($this->where)){
                user_error('危险操作：修改数据时无筛选条件', E_USER_ERROR);
            }

            $this->sql = [
                'px'            => ' UPDATE ',
                'table'         => $this->_getTable(),
                'extend_table'  => $this->_getExtendTable(),
                ' SET ',
                'data'          => $this->_getData('update'),
                'where'         => $this->_getWhere()
            ];

            // die(implode(' ', $this->sql));

            $statment = $this->_DAO->prepare(implode(' ', $this->sql));
            $statment->execute($this->param);
            $this->reset();
            return $statment->rowCount();
        }

        /**
         * @return int
         */
        public function insert(): int
        {
            if(empty($this->table) || count($this->table) != 2)
                user_error('表名错误', E_USER_ERROR);

            $this->sql = [
                'px'            => " INSERT INTO ",
                'table'         => $this->format_name($this->config['prefix'] . $this->table[0]),
                'data'          => $this->_getData('insert')
            ];

            // die(implode(' ', $this->sql));

            $statment = $this->_DAO->prepare(implode(' ', $this->sql));
            $statment->execute($this->param);
            $this->reset();
            return $this->_DAO->lastInsertId();
        }

        /**
         * @return int
         */
        public function delete(): int
        {
            if(empty($this->table) || count($this->table) != 2)
                user_error('表名错误', E_USER_ERROR);

            $this->sql = [
                " DELETE FROM ",
                'table'         => $this->_getTable(),
                'where'         => $this->_getWhere()
            ];

            // die(implode(' ', $this->sql));

            $statment = $this->_DAO->prepare(implode(' ', $this->sql));
            $statment->execute($this->param);
            $this->reset();
            return $statment->rowCount();
        }

        /**
         * @return string
         */
        private function _getField() : string
        {
            if(empty($this->field))
                return ' * ';

            $field_sql_part = '';
            foreach ($this->field as $key => $vo){
                if(str_contains($vo, ':')){
                    $vo = explode(':', $vo);
                }
                if($key == 0){
                    if(is_array($vo)){
                        $field_sql_part .= $this->format_name($vo[0]) . ' AS ' . $this->format_name($vo[1]);
                    }else{
                        $field_sql_part .= $this->format_name($vo);
                    }
                }else{
                    if(is_array($vo)){
                        $field_sql_part .= ', ' . $this->format_name($vo[0]) . ' AS ' . $this->format_name($vo[1]);
                    }else{
                        $field_sql_part .= ', ' . $this->format_name($vo);
                    }
                }
            }

            return $field_sql_part;
        }

        private function _getTable() : string
        {
            return implode('', [
                $this->format_name($this->config['prefix'] . $this->table[0]),
                ' AS ',
                $this->format_name($this->table[1])
            ]);
        }

        /**
         * @return string
         */
        private function _getWhere() : string
        {
            $where_sql_part = ' ' . (count($this->where) ? ' WHERE ' : '');
            $end = false;
            foreach($this->where as $key => $item){
                if($item == end($this->where)){
                    $end = true;
                }
                if(empty($item[0]) || empty($item[1]))
                    user_error('查询参数错误', E_USER_ERROR);

                if(gettype($item['1']) == 'array'){
                    $where_sql_part .= $this->comparison($item);
                    continue;
                }else{
                    $item['1'] = " ? ";
                }

                switch (count($item)){
                    case 2 :
                        $where_sql_part .= implode(' ', [
                            $this->format_name($item[0]),
                            '=',
                            $item['1'],
                            $end ? ' ' : ' AND '
                        ]);
                        break;
                    case 3 :
                        if(empty($item[2]))
                            user_error('查询参数错误', E_USER_ERROR);
                        $where_sql_part .= implode(' ', [
                            $this->format_name($item[0]),
                            $item[2],
                            $item['1'],
                            $end ? ' ' : ' AND '
                        ]);
                        break;
                    case 4 :
                        if(empty($item[2]) || empty($item[3]))
                            user_error('查询参数错误', E_USER_ERROR);
                        $where_sql_part .= implode(' ', [
                            $this->format_name($item[0]),
                            $item[2],
                            $item['1'],
                            $end ? ' ' : ' ' . $item[3] . ' '
                        ]);
                        break;
                    case 5 :
                        if(empty($item[0]) || empty($item[1]) || empty($item[2]) || empty($item[3]) || empty($item[4]))
                            user_error('查询参数错误', E_USER_ERROR);
                        if(!in_array($item[4], ['start', 'end']))
                            user_error('查询参数第5位仅能为start或end', E_USER_ERROR);
                        if($item['4'] == 'start'){
                            $where_sql_part .= ' ( ';
                        }
                        $where_sql_part .= implode(' ', [
                            $this->format_name($item[0]),
                            $item[2],
                            $item['1'],
                            $item['4'] == 'end' ? ' ' : $item[3] . ' ',
                        ]);
                        if($item['4'] == 'end'){
                            $where_sql_part .= ' ) ' . ($end ? '' : " {$item[3]} ");
                        }
                        break;
                    default:
                        user_error('查询参数错误: 单个查询条件数组长度应为3~5个', E_USER_ERROR);
                        break;
                }
                $this->param[] = $this->where[$key][1];
            }

            return $where_sql_part;
        }

        private function _getExtendTable() : string
        {
            $ext_tab_sql_part = '';
            foreach($this->extend_table as $vo){
                $ext_tab_sql_part .= implode(' ', [
                    ' LEFT JOIN ',
                    $this->format_name($this->config['prefix'] . $vo['0']),
                    ' AS ',
                    $this->format_name($vo[1]) . ' ON ' . $vo[2]
                ]);
            }
            return $ext_tab_sql_part;
        }

        private function _getGroup(): string
        {
            if(!empty($this->group)){
                return ' GROUP BY ' . $this->group . ' ';
            }else{
                return '';
            }
        }

        private function _getOrder() : string
        {
            if(!empty($this->order)){
                $order_sql_part = ' ORDER BY  ';
                foreach($this->order as $key => $vo){
                    $order_sql_part .= " " . $this->format_name($key) . ' ' . $vo . " ";
                }
                return $order_sql_part;
            }else{
                return '';
            }
        }

        private function _getLimit() : string
        {
            if(!empty($this->page) && !empty($this->limit)){
                return ' LIMIT ' . (($this->page - 1) * $this->limit) . ' , ' . $this->limit;
            }else{
                return '';
            }
        }

        private function _getData($type) : string
        {
            if(empty($this->data)){
                user_error('数据更新或插入时无数据', E_USER_ERROR);
            }

            $data_sql_part = '';
            if($type == 'insert'){
                $fields = array_keys($this->data);
                $vals = array_values($this->data);

                foreach($fields as $key => $item){
                    $fields[$key] = $this->format_name($item);
                    $this->param[] = $vals[$key];
                }
                $data_sql_part .= ' ( ' . implode(',', $fields) . ' ) ';
                $data_sql_part .= ' VALUES ( ' . implode(',', str_split(str_repeat('?', count($vals)))) . ' ) ';
            }else if($type == 'update'){
                foreach($this->data as $key => $item){
                    if(end($this->data) == $item){
                        $data_sql_part .= $this->format_name($key) . " = ? ";
                    }else{
                        $data_sql_part .= $this->format_name($key) . " = ? , ";
                    }
                    $this->param[] = $item;
                }
            }else{
                user_error('系统错误', E_USER_ERROR);
            }
            return $data_sql_part;
        }
    }
}