<?php

namespace System\Lib\Dao;

interface DaoInterface
{


    /**
     * 初始化数据库连接驱动
     * @param array $config
     * @return DaoInterface
     */
    public static function init(array $config): DaoInterface;

    /**
     * 连接数据库
     * @return void
     */
    public function connect(): void;

    /**
     * @param $fieldList
     * @return DaoInterface
     */
    public function setField($fieldList): DaoInterface;

    /**
     * @param string $tablename
     * @param string $alias
     * @return DaoInterface
     */
    public function setTable(string $tablename, string $alias = 'A'): DaoInterface;

    /**
     * @param string $tablename [表名]
     * @param string $alias [别名]
     * @param string $relation [关系]
     * @return DaoInterface
     */
    public function setExtendTable(string $tablename, string $alias, string $relation): DaoInterface;

    /**
     * @param array ...$whereParams
     * @return DaoInterface
     */
    public function setWhere(array ...$whereParams): DaoInterface;

    /**
     * @param array $data
     * @return DaoInterface
     */
    public function setData(array $data): DaoInterface;

    public function setGroup(string $group_field): DaoInterface;

    public function setOrder(array $order_field = []): DaoInterface;

    public function setPage(int $page = 1, int $limit = 10): DaoInterface;

    public function query() : array;

    public function first() : array;

    public function count() : int;

    public function update() : int;

    public function insert() : int;

    public function delete() : int;

}