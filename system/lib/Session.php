<?php
/**
 * Created by PhpStorm
 * User: Administrator
 * Date: 2022/12/3
 * Time: 2:25
 * docs:
 */

namespace System\Lib {
    /**
     * Session类
     * Date: 2021/01/08
     */

    class Session {

        /**
         * @name: _init
         * @author: Administrator
         * @Time: 2022/12/3 2:27
         * @Desc: 初始化
         */
        static function _init() : void
        {
            ini_set('session.auto_start', 0);
        }

        /**
         * @name: start
         * @author: Administrator
         * @Time: 2022/12/3 2:28
         * @Desc: 启动Session
         */
        static function start() : void
        {
            session_start();
        }

        /**
         * @name: set
         * @param string $name
         * @param mixed $value
         * @param int|bool $time
         * @author: Administrator
         * @Time: 2022/12/12 18:59
         * @Desc: 设置Session
         */
        public static function set(string $name, mixed $value, int|bool $time = false) : void
        {
            if(empty($time)){
                $time = 60 * 60; //默认值
            }

            $_SESSION[$name . '_Expires'] = time() + $time;
            $_SESSION[$name] = $value;
        }

        /**
         * @name: get
         * @param $name
         * @return mixed|null
         * @author: Administrator
         * @Time: 2022/12/3 2:28
         * @Desc: 获取Session值
         */
        public static function get($name) : mixed
        {
            //检查Session是否已过期
            if(isset($_SESSION[$name.'_Expires']) && $_SESSION[$name.'_Expires']>time()){
                return $_SESSION[$name];
            }else{
                Session::clear($name);
                return null;
            }
        }

        /**
         * @name: setDomain
         * @param $sessionDomain
         * @return string
         * @author: Administrator
         * @Time: 2022/12/3 2:29
         * @Desc: 设置Session Domain
         */
        public static function setDomain($sessionDomain = null) : string
        {
            $return = ini_get('session.cookie_domain');
            if(!empty($sessionDomain)) {
                ini_set('session.cookie_domain', $sessionDomain);//跨域访问Session
            }
            return $return;
        }

        /**
         * @name: clear
         * @param $name
         * @author: Administrator
         * @Time: 2022/12/3 2:29
         * @Desc: 删除指定Session值
         */
        public static function clear($name) : void
        {
            unset($_SESSION[$name]);
            unset($_SESSION[$name . '_Expires']);
        }

        /**
         * @name: destroy
         * @author: Administrator
         * @Time: 2022/12/3 2:30
         * @Desc: 重置销毁Session
         */
        public static function destroy() : void
        {
            unset($_SESSION);
            session_destroy();
        }

        /**
         * @name: sessionId
         * @param $id
         * @return bool|string
         * @author: Administrator
         * @Time: 2022/12/3 2:31
         * @Desc: 获取或设置Session id
         */
        static function sessionId( $id = null ) : bool|string
        {
            return session_id($id);
        }
    }
}