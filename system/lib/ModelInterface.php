<?php
namespace System\Lib{
    interface ModelInterface
    {
        /**
         * @param int $page
         * @param int $limit
         * @param array $where
         * @return array
         */
        public function list(int $page = 1, int $limit = 10, array $where = []) : array;

        /**
         * @param array $where
         * @return int
         */
        public function count(array $where = []) : int;

        /**
         * @param array $where
         * @return array
         */
        public function info(array $where) : array;

        /**
         * @param array $data
         * @return int
         */
        public function insert(array $data) : int;

        /**
         * @param array $data
         * @param array $where
         * @return int
         */
        public function update(array $data, array $where) : int;

        /**
         * @param array $where
         * @return int
         */
        public function delete(array $where) : int;
    }
}